  var app = angular.module("pmcapp", ['ui.router', 'ngAnimate']);

  app.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
      $urlRouterProvider.otherwise("/home");
      $stateProvider.state('home', {
          url: '/home',
          templateUrl: 'partials/home.html',
          controller: 'HomeCtrl'
      }).state('requestcard', {
          url: '/requestcard',
          templateUrl: 'partials/requestcard.html',
          controller: 'RequestCtrl'
      }).state('pmcback', {
          url: '/pmcback',
          templateUrl: 'partials/pmcback.html',
          controller: 'PmcBackCtrl'
      })


      .state('activatecard', {
          url: '/activatecard',
          templateUrl: 'partials/activatecard.html',
          controller: 'ActivateCtrl'
      }).state('resetpin', {
          url: '/resetpin',
          templateUrl: 'partials/resetpin.html',
          controller: 'ResetCtrl'
      }).state('blockcard', {
          url: '/blockcard',
          templateUrl: 'partials/blockcard.html',
          controller: 'BlockCtrl'
      }).state('passbook', {
          url: '/passbook',
          templateUrl: 'partials/passbook.html',
          controller: 'PassbookCtrl'
      }).state('savings', {
          url: '/savings',
          templateUrl: 'partials/savings.html',
          controller: 'SavingsCtrl'
      }).state('profile', {
          url: '/profile',
          templateUrl: 'partials/profile.html',
          controller: 'ProfileCtrl'
      }).state('childhome', {
          url: '/childhome',
          templateUrl: 'partials/childhome.html',
          controller: 'ChildHomeCtrl'
      }).state('yourbalance', {
          url: '/yourbalance',
          templateUrl: 'partials/yourbalance.html',
          controller: 'YourBalanceCtrl'
      }).state('requestmoney', {
          url: '/requestmoney',
          templateUrl: 'partials/requestmoney.html',
          controller: 'RequestMoneyCtrl'
      }).state('childgoals', {
          url: '/childgoals',
          templateUrl: 'partials/childgoals.html',
          controller: 'ChildGoalsCtrl'
      }).state('setlimits', {
          url: '/setlimits',
          templateUrl: 'partials/setlimits.html',
          controller: 'SetLimitsCtrl'
      })
            }]);
  app.service('anchorSmoothScroll', function () {

      this.scrollTo = function (eID) {

          // This scrolling function 
          // is from http://www.itnewb.com/tutorial/Creating-the-Smooth-Scroll-Effect-with-JavaScript

          var startY = currentYPosition();
          var stopY = elmYPosition(eID);
          var distance = stopY > startY ? stopY - startY : startY - stopY;
          if (distance < 100) {
              scrollTo(0, stopY);
              return;
          }
          var speed = Math.round(distance / 100);
          if (speed >= 20) speed = 20;
          var step = Math.round(distance / 25);
          var leapY = stopY > startY ? startY + step : startY - step;
          var timer = 0;
          if (stopY > startY) {
              for (var i = startY; i < stopY; i += step) {
                  setTimeout("window.scrollTo(0, " + leapY + ")", timer * speed);
                  leapY += step;
                  if (leapY > stopY) leapY = stopY;
                  timer++;
              }
              return;
          }
          for (var i = startY; i > stopY; i -= step) {
              setTimeout("window.scrollTo(0, " + leapY + ")", timer * speed);
              leapY -= step;
              if (leapY < stopY) leapY = stopY;
              timer++;
          }

          function currentYPosition() {
              // Firefox, Chrome, Opera, Safari
              if (self.pageYOffset) return self.pageYOffset;
              // Internet Explorer 6 - standards mode
              if (document.documentElement && document.documentElement.scrollTop)
                  return document.documentElement.scrollTop;
              // Internet Explorer 6, 7 and 8
              if (document.body.scrollTop) return document.body.scrollTop;
              return 0;
          }

          function elmYPosition(eID) {
              var elm = document.getElementById(eID);
              var y = elm.offsetTop;
              var node = elm;
              while (node.offsetParent && node.offsetParent != document.body) {
                  node = node.offsetParent;
                  y += node.offsetTop;
              }
              return y;
          }

      };

  });
  app.factory('checkSession', function ($http, $rootScope, $location) {

      $rootScope.authenticated = false;

      var promise = $http.get('api/pmcSession').success(function (results) {
          if (results.parents_user_id) {
              $rootScope.authenticated = true;
              $rootScope.puser = results;
              puserid = $rootScope.puser.parents_user_id;


          } else {
              $rootScope.authenticated = false;
              $rootScope.puser = null;


          }
      });
      return promise;
  });



  app.run(function ($rootScope, checkSession, $location, anchorSmoothScroll, $window) {

      $rootScope.page = null;
      $rootScope.$on("$stateChangeStart", function (event, next, current) {

          checkSession.then(function (result) {
              if (result.data.parents_user_id != null && result.data.parents_user_id != "") {

                  //    $scope.checkLiked();

              }
          });

      });

  });




  app.controller("CommonCtrl", ["$scope", "$http", "$controller", "$rootScope", "checkSession", "$location", "$interval", function ($scope, $http, $controller, $rootScope, checkSession, $location, $interval) {
      $scope.logout = function () {
          $http.get('api/pmcLogout').success(function (data) {

              $location.path('home');
              location.reload();

          }).error(function (data) {



          });
      }
      $scope.login = function () {
          $scope.errorMessage = null;
          $scope.showLoader = true;
          $http.post('api/pmcLogin', {
              customer: $scope.customer
          }).success(function (data) {
              if (data.status == "error") {
                  $scope.isError = true;
                  $scope.errorMessage = data.message;
                  $scope.showLoader = false;
                  $rootScope.authenticated = false;
              } else {
                  $rootScope.authenticated = true;
                  $scope.isError = false;
                  $scope.showLoader = false;
                  $rootScope.puser = data;
                  $('#myModal').modal("hide");

              }
          }).error(function (data) {
              $rootScope.uid = null;
              $rootScope.authenticated = false;
          });
      }

      $scope.forgotPassword = function () {
          $('#loading').modal({
              backdrop: 'static',
              keyboard: false
          });
          $http.get('api/forgotPassword/' + $scope.f_wizkid).success(function (data) {
              if (data == "success") {
                  $('#loginModal').modal("hide");
                  window.location = "#/login";
                  alert('Please check the email provided');
                  $scope.isForgot = false;
              } else {
                  alert('Please enter a valid wizkid name');
                  $scope.f_wizkid = '';
              }
              $('#loading').modal("hide");
          }).error(function (data) {
              $('#loading').modal("hide");
              $rootScope.uid = null;
              $rootScope.authenticated = false;
          });
      }



}]);
  app.controller("HomeCtrl", ["$scope", "$http", "$state", "$controller", "$rootScope", function ($scope, $http, $state, $controller, $rootScope) {
      $scope.getKids = function () {

          $http.get('api/getKids/' + $rootScope.puser.parents_user_id).success(function (data) {

              $scope.kids = data;



          }).error(function (data) {


          });

      }

      if ($scope.authenticated) {
          $scope.getKids();
      }
      $rootScope.page = 'home';
      $controller('CommonCtrl', {
          $scope: $scope
      });
      $scope.reg = 1;
      $scope.step = 1;


      $scope.rechstep1 = function () {

          $scope.step = 2;

      }
      $scope.rechstep2 = function () {



      }
      $scope.tabClick = function (tabname, regvalue) {


          if (tabname === 'register') {
              $('.register-tab').addClass('tab-clicked-modal');
              $('.login-tab').removeClass('tab-clicked-modal');
          }

          if (tabname === 'login') {
              $('.login-tab').addClass('tab-clicked-modal');
              $('.register-tab').removeClass('tab-clicked-modal');
          }
      }

              }]);


  app.controller("RequestCtrl", ["$scope", "$http", "$state", "$controller", "$rootScope", function ($scope, $http, $state, $controller, $rootScope) {
      $scope.page = 'request';
      $controller('CommonCtrl', {
          $scope: $scope
      });
      $scope.step = 1;
      $scope.pog = 1;
      $scope.$watch('authenticated', function () {
          if ($scope.authenticated) {
              $scope.step = 2;
          } else {

              $scope.step = 1;
          }
      }, true);

      $scope.addParent = function () {
              if ($scope.isTnc) {
                  if ($scope.pname != null && $scope.pname != '' && $scope.pemail != null && $scope.pemail != '' && $scope.ppass != null && $scope.ppass != '' && $scope.pmob != null && $scope.pmob != '' && $scope.pgender != null && $scope.pgender != '') {
                      $http.get('api/addParent/' + $scope.pname + '/' + $scope.pemail + '/' + $scope.ppass + '/' + $scope.pmob + '/' + $scope.pgender).success(function (data) {

                          if (data.status == "error") {
                              $scope.isError = true;
                              $scope.errorMessage = data.message;
                              // alert(data.message);

                              $rootScope.authenticated = false;
                          } else {
                              $rootScope.authenticated = true;
                              $scope.isError = false;
                              $scope.errorMessage = null;
                              $scope.step = 2;
                              $rootScope.puser = data;
                              $('#myModal').modal("hide");

                          }


                      }).error(function (data) {
                          $scope.isError = true;
                          $scope.errorMessage = "Oops! Seems like you have logged in previously. Please Login if you have registered with us.";
                      });

                  } else {
                      $scope.isError = true;
                      $scope.errorMessage = "Please add all the fields.";
                  }
              } else {

                  $scope.isError = true;
                  $scope.errorMessage = "Please check the Terms and Conditions before going ahead";
              }

          }
          //      $scope.addKid = function () {
          //
          //          $http.get('api/addKid/' + $scope.name + '/' + $scope.dob + '/' + $scope.pass + '/' + $scope.mob + '/' + $scope.email).success(function (data) {
          //              $scope.step = 3;
          //
          //          }).error(function (data) {
          //
          //
          //          });
          //
          //      }

      $scope.isNew = 'y';

      $scope.addAddress = function () {
          if ($scope.line1 != null && $scope.line1 != '' && $scope.line2 != null && $scope.line2 != '' && $scope.city != null && $scope.city != '' && $scope.state != null && $scope.state != '' && $scope.pin != null && $scope.pin != '') {
              $http.get('api/getKidId/' + $rootScope.puser.parents_user_id).success(function (data) {
                  $http.get('api/addAddress/' + $scope.line1 + '/' + $scope.line2 + '/' + $scope.city + '/' + $scope.state + '/' + $scope.pin + '/' + $rootScope.puser.parents_user_id + '/' + data.child_id).success(function (data) {
                      $scope.isError = false;
                      $scope.errorMessage = null;

                      var options = {
                          "key": "rzp_live_yZgpFE7e1VDXRI",
                          "amount": "100",
                          "name": "Shirsa Labs Pvt Ltd",
                          "description": "PMC Annual Membership",
                          "image": "images/pmc-scroll-logo.png",
                          "handler": function (response) {
                              alert(response.razorpay_payment_id);
                              $scope.step = 4;
                          },
                          "prefill": {

                              "email": $rootScope.puser != null ? $rootScope.puser.email : "",
                              "contact": $rootScope.puser != null ? $rootScope.puser.mobile_number : ""
                          },
                          "notes": {
                              "user_id": $rootScope.puser != null ? $rootScope.puser.parents_user_id : "Not Logged in"
                          },
                          "theme": {
                              "color": "#F37254"
                          }
                      };
                      var rzp1 = new Razorpay(options);
                      rzp1.open();
                      e.preventDefault();


                  }).error(function (data) {


                  });

              }).error(function (data) {


              });

          } else {
              $scope.isError = true;
              $scope.errorMessage = "Please add all the fields.";
          }

      }

      $scope.linkKid = function () {
          if ($scope.isNew == 'y') {

              if ($scope.kname != null && $scope.kname != '' && $scope.kdob != null && $scope.kdob != '' && $scope.kpass != null && $scope.kpass != '') {
                  $http.get('api/addNewKid/' + $scope.kname + '/' + $scope.kdob + '/' + $scope.kpass + '/' + $rootScope.puser.mobile_number + '/' + $rootScope.puser.email + '/' + $rootScope.puser.parents_user_id).success(function (data) {
                      $scope.step = 3;
                      $scope.isError = false;
                      $scope.errorMessage = null;
                  }).error(function (data) {
                      $scope.isError = true;
                      $scope.errorMessage = "Oops! your kid is already registered with us.";

                  });
              } else {
                  $scope.isError = true;
                  $scope.errorMessage = "Please add all the fields.";
              }


          } else {
              if ($scope.kname != null && $scope.kname != '' && $scope.kpass != null && $scope.kpass != '') {
                  $http.get('api/addKid/' + $scope.kname + '/' + $scope.kpass + '/' + $rootScope.puser.parents_user_id).success(function (data) {

                      if (data.status == "success") {
                          $scope.step = 3;
                      } else {
                          alert(data.message);
                      }

                  }).error(function (data) {


                  });
              } else {
                  $scope.isError = true;
                  $scope.errorMessage = "Please add all the fields.";
              }

          }


      }
      $(function () {
          $('#dpMonths').fdatepicker();
      });
              }]);
  app.controller("ActivateCtrl", ["$scope", "$http", "$state", "$controller", "$rootScope", function ($scope, $http, $state, $controller, $rootScope) {
      $scope.page = 'activate';
      $controller('CommonCtrl', {
          $scope: $scope
      });
      $scope.step = 1;
      $scope.pog = 1;


      $scope.linkKid = function () {
          if ($scope.isNew == 'y') {

              if ($scope.kname != null && $scope.kname != '' && $scope.kdob != null && $scope.kdob != '' && $scope.kpass != null && $scope.kpass != '') {
                  $http.get('api/addNewKidActivate/' + $scope.kname + '/' + $scope.kdob + '/' + $scope.kpass + '/' + $rootScope.puser.mobile_number + '/' + $rootScope.puser.email + '/' + $rootScope.puser.parents_user_id).success(function (data) {
                      $scope.step = 3;

                  }).error(function (data) {
                      alert("Oops! your kid is already registered with us.");

                  });
              } else {
                  alert("Please add all the fields.");
              }


          } else {
              if ($scope.kname != null && $scope.kname != '' && $scope.kpass != null && $scope.kpass != '') {
                  $http.get('api/addKidActivate/' + $scope.kname + '/' + $scope.kpass + '/' + $rootScope.puser.parents_user_id).success(function (data) {

                      if (data.status == "success") {
                          $scope.step = 3;
                      } else {
                          alert(data.message);
                      }

                  }).error(function (data) {


                  });
              } else {
                  alert("Please add all the fields.");
              }

          }


      }



      $scope.addParentActivate = function () {
          if ($scope.isTnc) {
              if ($scope.card1 != null && $scope.card1 != "" && $scope.card2 != null && $scope.card2 != "" && $scope.card3 != null && $scope.card3 != "" && $scope.card4 != null && $scope.card4 != "" && $scope.pname != null && $scope.pname != '' && $scope.pemail != null && $scope.pemail != '' && $scope.ppass != null && $scope.ppass != '' && $scope.pmob != null && $scope.pmob != '' && $scope.pgender != null && $scope.pgender != '') {

                  $scope.card = $scope.card1 + "" + $scope.card2 + "" + $scope.card3 + "" + $scope.card4;
                  $http.get('api/addParentActivate/' + $scope.card + '/' + $scope.pname + '/' + $scope.pemail + '/' + $scope.ppass + '/' + $scope.pmob + '/' + $scope.pgender).success(function (data) {

                      if (data.status == "error") {
                          $scope.isError = true;
                          $scope.errorMessage = data.message;
                          alert(data.message);

                          $rootScope.authenticated = false;
                      } else {
                          $rootScope.authenticated = true;
                          $scope.isError = false;
                          $scope.step = 2;
                          $rootScope.puser = data;
                          $('#myModal').modal("hide");

                      }


                  }).error(function (data) {

                      alert("Oops! Seems like you have logged in previously. Please Login if you have registered with us.");
                  });

              } else {
                  alert("Please add all the fields.");
              }
          } else {

              alert("Terms and Conditions not checked");
          }

      }

              }]);
  app.controller("ResetCtrl", ["$scope", "$http", "$state", "$controller", "$rootScope", function ($scope, $http, $state, $controller, $rootScope) {
      $controller('CommonCtrl', {
          $scope: $scope
      });
      $scope.reset = 1;

      $scope.getKids = function () {

          $http.get('api/getKids/' + $rootScope.puser.parents_user_id).success(function (data) {

              $scope.kids = data;

          }).error(function (data) {


          });

      }


      $scope.getKids();

      $scope.isReset = false;
      $scope.resetPin = function (i, id) {


          if ($scope.kids[i].old_pin != null && $scope.kids[i].old_pin != "" && $scope.kids[i].new_pin != null && $scope.kids[i].new_pin != "" && $scope.kids[i].new_pin_2 != null && $scope.kids[i].new_pin_2 != null) {
              $http.get('api/resetPin/' + $scope.kids[i].child_id + '/' + $scope.kids[i].old_pin + '/' + $scope.kids[i].new_pin).success(function (data) {

                  alert(data.message);
                  $scope.getKids();
                  $scope.isReset = true;
                  $scope.kids[i].new_pin = null;
                  $scope.kids[i].old_pin = null;
                  $scope.kids[i].new_pin_2 = null;


              }).error(function (data) {


              });
          } else {
              alert('Please enter the fields ');

          }


      }



              }]);
  app.controller("BlockCtrl", ["$scope", "$http", "$state", "$controller", "$rootScope", function ($scope, $http, $state, $controller, $rootScope) {
      $controller('CommonCtrl', {
          $scope: $scope
      });
      $scope.block = 1;
      $scope.getKids = function () {

          $http.get('api/getKids/' + $rootScope.puser.parents_user_id).success(function (data) {

              $scope.kids = [];

              for (var i = 0; i < data.length; i++) {
                  if (data[i].card_number != "" && data[i].card_number != null)
                      $scope.kids.push(data[i]);

              }

          }).error(function (data) {


          });

      }
      $scope.reason = null;

      $scope.getKids();

      $scope.lockCard = function (i, id) {


          if ($scope.kids[i].reason != null) {
              $http.get('api/lockCard/' + $scope.kids[i].child_id + '/' + $scope.kids[i].reason).success(function (data) {
                  $scope.getKids();



              }).error(function (data) {


              });
          } else {
              alert('Please select reason');

          }


      }
      $scope.selectReason = function (i, r) {

          $scope.kids[i].reason = r;
      }
      $scope.unlockCard = function (i, id) {

          if ($scope.kids[i].reason != null) {
              $http.get('api/unlockCard/' + $scope.kids[i].child_id + '/' + $scope.kids[i].reason).success(function (data) {
                  $scope.getKids();



              }).error(function (data) {


              });
          } else {
              alert('Please select reason');

          }



      }
              }]);

  app.controller("PmcBackCtrl", ["$scope", "$http", "$state", "$controller", "$rootScope", function ($scope, $http, $state, $controller, $rootScope) {
      $controller('CommonCtrl', {
          $scope: $scope
      });
      $scope.menu = 'users';
      $scope.block = 1;
      $scope.getKids = function () {
          $scope.menu = 'users';
          $http.get('api/getPmcUsers').success(function (data) {


              $scope.pmc_users = data;



          }).error(function (data) {


          });

      }
      $scope.assignCard = function (childId, parentsUserId) {

          $http.get('api/assignCard/' + childId + '/' + parentsUserId).success(function (data) {


              $scope.getKids();


          }).error(function (data) {


          });

      }
      $scope.getKids();
      $scope.getCards = function () {
          $scope.menu = 'cards';
          $http.get('api/getPmcCards').success(function (data) {

              $scope.cards = data;



          }).error(function (data) {


          });

      }
      $scope.reason = null;

      $scope.openReceiveModal = function (p) {
          $scope.selectedUser = p;
          $scope.type = "Received";
          $('#receive').modal('show');

      }
      $scope.openSpendModal = function (p) {
          $scope.selectedUser = p;
          $scope.type = "Spent";
          $('#receive').modal('show');

      }


      $scope.addTransaction = function () {
          $http.get('api/addTransaction/' + $scope.selectedUser.card_number + '/' + $scope.amount + '/' + $scope.vendor + '/' + $scope.type + '/' + $scope.txid).success(function (data) {
              alert("Transaction added successfully");



          }).error(function (data) {


          });


      }
      $scope.lockCard = function (id) {



          $http.get('api/lockCard/' + id + '/backend').success(function (data) {
              $scope.getKids();



          }).error(function (data) {


          });


      }

      $scope.unlockCard = function (id) {

          $http.get('api/unlockCard/' + id + '/backend').success(function (data) {
              $scope.getKids();



          }).error(function (data) {


          });




      }
              }]);


  app.controller("PassbookCtrl", ["$scope", "$http", "$state", "$controller", "$rootScope", function ($scope, $http, $state, $controller, $rootScope) {
      $controller('CommonCtrl', {
          $scope: $scope
      });
      $scope.trans = 1;
      $scope.fliterClick = function (tabname, transvalue) {

          if (tabname === 'all') {
              $('.filters-diff-all').addClass('filters-selected');
              $('.filters-diff-recv').removeClass('filters-selected');
              $('.filters-diff-spent').removeClass('filters-selected');
          }

          if (tabname === 'recv') {
              $('.filters-diff-recv').addClass('filters-selected');
              $('.filters-diff-all').removeClass('filters-selected');
              $('.filters-diff-spent').removeClass('filters-selected');
          }
          if (tabname === 'spent') {
              $('.filters-diff-spent').addClass('filters-selected');
              $('.filters-diff-all').removeClass('filters-selected');
              $('.filters-diff-recv').removeClass('filters-selected');
          }


      }


      $scope.getKids = function () {

          $http.get('api/getKids/' + $rootScope.puser.parents_user_id).success(function (data) {

              $scope.kids = data;
              $scope.selectedKid = data[0];
              $scope.child_index = 0;
              $scope.getPassbookData('all', $scope.child_index);

          }).error(function (data) {


          });

      }


      $scope.getKids();
      $scope.getPassbookData = function (type, i) {
          $scope.child_index = i;
          $http.get('api/getPassbookData/' + type + '/' + $scope.kids[i].child_id).success(function (data) {
              $scope.pass = data;
              $scope.selectedKid = $scope.kids[i];
          }).error(function (data) {
              alert("Oops! We are facing some technical issues at the moment");
          });


      }









              }]);
  app.controller("SavingsCtrl", ["$scope", "$http", "$state", "$controller", function ($scope, $http, $state, $controller) {
      $controller('CommonCtrl', {
          $scope: $scope
      });
      $scope.sav = 1;
      $scope.fliterClick = function (tabname, transvalue) {

          if (tabname === 'all') {
              $('.filters-diff-all').addClass('sav-filters-selected');
              $('.filters-diff-curr').removeClass('sav-filters-selected');
              $('.filters-diff-achv').removeClass('sav-filters-selected');
              $('.filters-diff-miss').removeClass('sav-filters-selected');
          }

          if (tabname === 'curr') {
              $('.filters-diff-curr').addClass('sav-filters-selected');
              $('.filters-diff-all').removeClass('sav-filters-selected');
              $('.filters-diff-achv').removeClass('sav-filters-selected');
              $('.filters-diff-miss').removeClass('sav-filters-selected');
          }
          if (tabname === 'achv') {
              $('.filters-diff-achv').addClass('sav-filters-selected');
              $('.filters-diff-all').removeClass('sav-filters-selected');
              $('.filters-diff-curr').removeClass('sav-filters-selected');
              $('.filters-diff-miss').removeClass('sav-filters-selected');
          }
          if (tabname === 'miss') {
              $('.filters-diff-miss').addClass('sav-filters-selected');
              $('.filters-diff-all').removeClass('sav-filters-selected');
              $('.filters-diff-curr').removeClass('sav-filters-selected');
              $('.filters-diff-achv').removeClass('sav-filters-selected');
          }


      }

      $("#flat-slider")
          .slider({
              max: 50,
              min: 0,
              range: true,
              values: [15, 35]
          })
          /*.slider("pips", {
              first: "pip",
              last: "pip"
          })*/
      ;

      /*
      $("#flat-slider-vertical-1, #flat-slider-vertical-2, #flat-slider-vertical-3")
          .slider({
              max: 25,
              min: 0,
              range: "min",
              value: 20,
              orientation: "vertical"
          })
          .slider("pips", {
              first: "pip",
              last: "pip"
          })
          .slider("float");
      */
              }]);
  app.controller("ProfileCtrl", ["$scope", "$http", "$state", "$controller", "$rootScope", function ($scope, $http, $state, $controller, $rootScope) {
      $controller('CommonCtrl', {
          $scope: $scope
      });

      $scope.getProfileData = function () {
          $http.get('api/getProfileData/' + $rootScope.puser.parents_user_id).success(function (data) {
              $scope.p = data;

          }).error(function (data) {
              alert("Oops! We are facing some technical issues at the moment");
          });


      }
      $scope.getProfileData();
              }]);
  app.controller("ChildHomeCtrl", ["$scope", "$http", "$state", "$controller", function ($scope, $http, $state, $controller) {
      $controller('CommonCtrl', {
          $scope: $scope
      });
      $scope.scrollToDiv = function (id) {
          // event.preventDefault();  
          $('html,body').animate({
              scrollTop: $(id).offset().top
          }, 1000);
      }
              }]);
  app.controller("YourBalanceCtrl", ["$scope", "$http", "$state", "$controller", function ($scope, $http, $state, $controller) {
      $controller('CommonCtrl', {
          $scope: $scope
      });
      $scope.trans = 1;
      $scope.fliterClick = function (tabname, transvalue) {

          if (tabname === 'all') {
              $('.filters-diff-all').addClass('filters-selected');
              $('.filters-diff-recv').removeClass('filters-selected');
              $('.filters-diff-spent').removeClass('filters-selected');
          }

          if (tabname === 'recv') {
              $('.filters-diff-recv').addClass('filters-selected');
              $('.filters-diff-all').removeClass('filters-selected');
              $('.filters-diff-spent').removeClass('filters-selected');
          }
          if (tabname === 'spent') {
              $('.filters-diff-spent').addClass('filters-selected');
              $('.filters-diff-all').removeClass('filters-selected');
              $('.filters-diff-recv').removeClass('filters-selected');
          }


      }

              }]);
  app.controller("RequestMoneyCtrl", ["$scope", "$http", "$state", "$controller", function ($scope, $http, $state, $controller) {
      $controller('CommonCtrl', {
          $scope: $scope
      });

              }]);
  app.controller("ChildGoalsCtrl", ["$scope", "$http", "$state", "$controller", function ($scope, $http, $state, $controller) {
      $controller('CommonCtrl', {
          $scope: $scope
      });
      $scope.sav = 1;
      $scope.fliterClick = function (tabname, transvalue) {

          if (tabname === 'all') {
              $('.filters-diff-all').addClass('sav-filters-selected');
              $('.filters-diff-curr').removeClass('sav-filters-selected');
              $('.filters-diff-achv').removeClass('sav-filters-selected');
              $('.filters-diff-miss').removeClass('sav-filters-selected');
          }

          if (tabname === 'curr') {
              $('.filters-diff-curr').addClass('sav-filters-selected');
              $('.filters-diff-all').removeClass('sav-filters-selected');
              $('.filters-diff-achv').removeClass('sav-filters-selected');
              $('.filters-diff-miss').removeClass('sav-filters-selected');
          }
          if (tabname === 'achv') {
              $('.filters-diff-achv').addClass('sav-filters-selected');
              $('.filters-diff-all').removeClass('sav-filters-selected');
              $('.filters-diff-curr').removeClass('sav-filters-selected');
              $('.filters-diff-miss').removeClass('sav-filters-selected');
          }
          if (tabname === 'miss') {
              $('.filters-diff-miss').addClass('sav-filters-selected');
              $('.filters-diff-all').removeClass('sav-filters-selected');
              $('.filters-diff-curr').removeClass('sav-filters-selected');
              $('.filters-diff-achv').removeClass('sav-filters-selected');
          }


      }
}]);
  app.controller("SetLimitsCtrl", ["$scope", "$http", "$state", "$controller", "$rootScope", function ($scope, $http, $state, $controller, $rootScope) {
      $controller('CommonCtrl', {
          $scope: $scope
      });
      $scope.getTransactionData = function () {
          $http.get('api/getTransactionData/' + $rootScope.puser.parents_user_id).success(function (data) {
              $scope.tpd = data.transactions_per_day;
              $scope.apt = data.amount_per_transaction;
          }).error(function (data) {
              alert("Oops! We are facing some technical issues at the moment");
          });


      }
      $scope.getTransactionData();
      $scope.$watch('tpd', function () {
          $http.get('api/updateTransactionNo/' + $rootScope.puser.parents_user_id + '/' + $scope.tpd).success(function (data) {

          }).error(function (data) {
              alert("Oops! We are facing some technical issues at the moment");
          });
      }, true);


      $scope.$watch('apt', function () {
          $http.get('api/updateAmountPerTransaction/' + $rootScope.puser.parents_user_id + '/' + $scope.apt).success(function (data) {

          }).error(function (data) {
              alert("Oops! We are facing some technical issues at the moment");
          });
      }, true);


              }]);
  app.directive('pmcFooter', function () {
      return {

          restrict: 'E',
          controller: function ($scope, $http) {

          },
          templateUrl: 'partials/pmc-footer.html'
      };
  });
  app.directive('pmcHeader', function () {
      return {
          restrict: 'E',
          controller: function ($scope, $rootScope) {


          },
          templateUrl: 'partials/pmc-header.html'
      };
  });