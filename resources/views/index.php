<!doctype html>
<html class="no-js" lang="en" ng-app="pmcapp" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pockett Money Card</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.css">
 <link rel="shortcut icon" sizes="16x16 24x24 32x32 48x48 64x64" href="images/fav_icon.ico">

    <!--      <link rel="stylesheet" href="css/material-foundation.css">-->
    <link rel="stylesheet" href="css/demo.css">
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/jquery.circular-carousel.css">
    <link rel="stylesheet" href="css/xeditable.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/jquery-ui-slider-pips.css">
    <link href="css/content_slider_style.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8">




    <!--<script type="text/javascript">
           jQuery(document).ready(function($) {
                $(".scroll").click(function(event){     
                    event.preventDefault();
                    $('html,body').animate({scrollTop:$(this.hash).offset().top}, 500);
                });

            });
	</script>-->

    <link href="css/doc_style.css" rel="stylesheet" type="text/css">
</head>

<body>
    <data-ui-view></data-ui-view>
</body>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<!--
    <script src="js/foundation/foundation-datepicker.js"></script>
    <script src="js/foundation/foundation-datepicker.min.js"></script>
    <script src="js/foundation/foundation.dropdown.js"></script>
-->
<script src="js/jquery.content_slider.min.js" type="text/javascript"></script>
<script src="js/jquery.mousewheel.js" type="text/javascript"></script>
<script src="js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
<script src="js/jquery.animate-colors.js" type="text/javascript"></script>
<script src="js/additional_content.js" type="text/javascript"></script>

<script src="js/jquery-ui.js" type="text/javascript"></script>

<script src="js/jquery-ui-slider-pips.js" type="text/javascript"></script>
<script src="js/jquery.circular-carousel.js"></script>
<script src="js/script.js"></script>
<script src="js/angular.min.js"></script>
<script src="js/angular-animate.min.js"></script>
<script src="js/angular-ui-router.min.js"></script>
<script src="js/xeditable.js" type="text/javascript"></script>
<script src="js/ripple.js" type="text/javascript"></script>

<script src="js/material-foundation.js" type="text/javascript"></script>
<script src="js/switches.js" type="text/javascript"></script>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script type="text/javascript">
    $(window).scroll(function () {

        var y = $(window).scrollTop();
        if (y > 200) {

            $('.header-menu-1').addClass('tucked-menu-show');
        } else {
            $('.header-menu-1').removeClass('tucked-menu-show');
        }

    });
    (function ($) {
        $(document).ready(function () {
            var image_array = new Array();
            image_array = [
                {
                    image: 'images/img1.jpg',
                    link_url: 'content/products/1big.jpg',
                    link_rel: 'prettyPhoto'
                    }
					, // image for the first layer, goes with the text from id="sw0"
                {
                    image: 'images/img2.jpg',
                    link_url: 'content/products/2big.jpg',
                    link_rel: 'prettyPhoto'
                    }
					, // image for the second layer, goes with the text from id="sw1"
                {
                    image: 'images/img3.jpg',
                    link_url: 'content/products/3big.jpg',
                    link_rel: 'prettyPhoto'
                    }
					, // image for the third layer, goes with the text from id="sw2"
                {
                    image: 'images/img4.jpg',
                    link_url: 'content/products/4big.jpg',
                    link_rel: 'prettyPhoto'
                    }
					, // ...
                {
                    image: 'images/img3.jpg',
                    link_url: 'content/products/5big.jpg',
                    link_rel: 'prettyPhoto'
                    }
                    , {
                    image: 'images/img1.jpg',
                    link_url: 'content/products/6big.jpg',
                    link_rel: 'prettyPhoto'
                    }
                    , {
                    image: 'images/img4.jpg',
                    link_url: 'content/products/7big.jpg',
                    link_rel: 'prettyPhoto'
                    }
			];
            $('#slider1').content_slider({ // bind plugin to div id="slider1"
                map: image_array, // pointer to the image map
                max_shown_items: 7, // number of visible circles
                hv_switch: 0, // 0 = horizontal slider, 1 = vertical
                active_item: 0, // layer that will be shown at start, 0=first, 1=second...
                wrapper_text_max_height: 410, // height of widget, displayed in pixels
                middle_click: 1, // when main circle is clicked: 1 = slider will go to the previous layer/circle, 2 = to the next
                under_600_max_height: 1200, // if resolution is below 600 px, set max height of content
                border_radius: -1, // -1 = circle, 0 and other = radius
                border_color: "#e3e3e3",
                arrow_color: "#D3D3D3",
                allow_shadow: 0,
                auto_play: 1
            });
        });
    })(jQuery);
</script>
<!--<script src="js/vendor/jquery.js"></script>-->
<!--<script src="js/vendor/what-input.js"></script>-->

<script src="js/app.js"></script>

</html>