<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PogNotificationExtension extends Model
{
    //
    protected $table = 'pog_notification_extension';
    
    protected $guarded = ['*'];
}