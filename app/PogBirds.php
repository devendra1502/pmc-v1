<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PogBirds extends Model
{
    //
    protected $table = 'pog_birds';
    
    protected $guarded = ['*'];
}