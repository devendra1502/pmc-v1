<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GeekosMultiplier extends Model
{
    //
     protected $table = 'geekos_multiplier';
    
    protected $guarded = ['*'];
    
    protected $primaryKey = 'geekos_multiplier_id';
}
