<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParentsChild extends \Eloquent
{
    
    protected $table = 'parents_child';
    
    protected $primaryKey = 'parents_child_id';
    
}