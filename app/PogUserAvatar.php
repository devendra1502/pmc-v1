<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PogUserAvatar extends Model
{
    protected $table = 'pog_user_avatar';
    
    protected $guarded = ['*'];
    
    protected $primaryKey = 'user_avatar_id';
}