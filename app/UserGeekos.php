<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserGeekos extends Model
{
    //
     protected $table = 'user_geekos';
    
    protected $guarded = ['*'];
    
    protected $primaryKey = 'user_geekos_id';
}
