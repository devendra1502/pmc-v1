<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('index');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});




Route::get('requestcard', 'PmcController@routeRequestCard');



Route::get('activatecard', 'PmcController@routeActivateCard');







//Users



Route::resource('users','UsersController');


Route::post('api/consumeContent/{cId}/{userId}/{ansFlag}', 'UsersController@consumeContent');


Route::post('api/submitPoll/{questId}/{userId}/{ansOption}', 'UsersController@submitPoll');

Route::get('api/checkprofilecompletion/{userProfileId}', 'UserProfileController@checkprofilecompletion');


Route::get('api/checkDataConsumption/{cId}/{userId}', 'UsersController@checkDataConsumption');

    
Route::get('api/setuserdata/{uHand}/{uDob}/{userProfileId}/{uFname}/{uLname}/{userId}', 'UsersController@setuserdata');


Route::get('api/getUserDataI/{userId}', 'UsersController@getUserDataI');


//Angular route redirects
Route::get('content/{id}/{slug}', 'UsersController@routeContent');
Route::get('search/{text}', 'UsersController@routeSearch');
Route::get('birddetail/{id}', 'UsersController@routeBirddetail');
Route::get('list/{type}', 'UsersController@routeList');
Route::get('sublist/{type}', 'UsersController@routeSubList');

Route::get('birds', 'UsersController@routeBirds');
Route::get('selectavatar', 'UsersController@routeSelectavatar');
Route::get('avatarselect', 'UsersController@routeAvatarselect');
Route::get('bog', 'UsersController@routeBog');
Route::get('parents', 'UsersController@routeParents');
Route::get('safety', 'UsersController@routeSafety');
Route::get('faq', 'UsersController@routeFaq');
Route::get('contact', 'UsersController@routeContact');
Route::get('partners', 'UsersController@routePartners');
Route::get('forgotpassword', 'UsersController@routeForgotpassword');
Route::get('changepassword', 'UsersController@routeChangepassword');
Route::get('challenge', 'UsersController@routeChallenge');
Route::get('membership', 'UsersController@routeMembership');
Route::get('redeem', 'UsersController@routeRedeem');
Route::get('performance', 'UsersController@routePerformance');
Route::get('shopping', 'UsersController@routeShopping');
Route::get('help', 'UsersController@routeHelp');
Route::get('email', 'UsersController@routeEmail');
Route::get('statistics', 'UsersController@routeStatistics');
Route::get('profile/id/{id}', 'UsersController@routeProfile');





Route::get('api/getUserDataN/{name}', 'UsersController@getUserDataN');

Route::post('api/verifyUser/{userId}', 'UsersController@verifyUser');


Route::get('api/wizkidNames', 'UsersController@wizkidNames');

Route::post('api/changePassword/{oldPassword}/{newPassword}/{userId}', 'UsersController@changePassword');


Route::get('api/forgotPassword/{email}', 'UsersController@forgotPassword');


//AuthenticationController

Route::post('api/pogLogin', 'AuthenticationController@pogLogin');


Route::get('api/addParent/{name}/{email}/{pass}/{mob}/{gender}', 'PmcController@addParent');
Route::get('api/addParentActivate/{card}/{name}/{email}/{pass}/{mob}/{gender}', 'PmcController@addParentActivate');

Route::get('api/addNewKid/{name}/{dob}/{pass}/{mob}/{email}/{parent}', 'PmcController@addNewKid');

Route::get('api/addKid/{name}/{pass}/{parent}', 'PmcController@addKid');

Route::get('api/addNewKidActivate/{name}/{dob}/{pass}/{mob}/{email}/{parent}', 'PmcController@addNewKidActivate');

Route::get('api/addKidActivate/{name}/{pass}/{parent}', 'PmcController@addKidActivate');
Route::get('api/addAddress/{line1}/{line2}/{city}/{state}/{pin}/{parent}/{kid}', 'PmcController@addAddress');

Route::get('api/updateTransactionNo/{parent}/{no}', 'PmcController@updateTransactionNo');

Route::get('api/updateAmountPerTransaction/{parent}/{no}', 'PmcController@updateAmountPerTransaction');

Route::get('api/getTransactionData/{parent}', 'PmcController@getTransactionData');

Route::get('api/addTransaction/{card}/{amount}/{vendor}/{type}/{txid}', 'PmcController@addTransaction');
    

Route::get('api/getPassbookData/{type}/{parentkid}', 'PmcController@getPassbookData');

Route::get('api/assignCard/{parentChildId}/{parentsUserId}', 'PmcController@assignCard');

Route::get('api/checkCard/{card}/{amount}', 'PmcController@checkCard');

Route::get('api/getProfileData/{parent}', 'PmcController@getProfileData');
Route::get('api/getKidProfileData/{parent}', 'PmcController@getKidProfileData');

Route::get('api/isWizkidExist/{wizkid}', 'PmcController@isWizkidExist');

Route::get('api/lockCard/{id}/{reason}', 'PmcController@lockCard');

Route::get('api/unlockCard/{id}/{reason}', 'PmcController@unlockCard');
Route::get('api/resetPin/{id}/{old_pin}/{new_pin}', 'PmcController@resetPin');

Route::get('api/getKids/{parent}', 'PmcController@getKids');


Route::get('api/getPmcUsers', 'PmcController@getPmcUsers');
Route::get('api/getPmcCards', 'PmcController@getPmcCards');


Route::get('api/getKidId/{parent}', 'PmcController@getKidId');


Route::get('api/linkParentKid/{parent}/{kid}', 'PmcController@linkParentKid');

Route::get('api/paymentSuccess/{parent}/{kid}', 'PmcController@paymentSuccess');


Route::post('api/pmcLogin', 'AuthenticationController@pmcLogin');


Route::post('api/pogSignup', 'AuthenticationController@pogSignup');

Route::get('api/pogLogout', 'AuthenticationController@pogLogout');

Route::get('api/pmcLogout', 'AuthenticationController@pmcLogout');

Route::get('api/pogSession', 'AuthenticationController@pogSession');
Route::get('api/pmcSession', 'AuthenticationController@pmcSession');


Route::post('api/pogIsWizkidExist', 'AuthenticationController@pogIsWizkidExist');

//PogContent
Route::get('api/getPageList/{type}/{category}', 'PogContentController@index');
//PogStatistics
Route::get('api/getStatisticsByDate/{to}', 'PogContentController@getStatisticsByDate');

Route::get('api/getStatistics', 'PogContentController@getStatistics');




Route::get('api/getFeaturedContent/{type}', 'PogContentController@getFeaturedContent');


Route::get('api/getFeaturedVideos/home', 'PogContentController@getFeaturedVideos');

Route::get('api/getFeaturedActivities/home', 'PogContentController@getFeaturedActivities');

Route::get('api/getFeaturedGames/home', 'PogContentController@getFeaturedGames');

Route::get('api/getSearchPageList/{searchText}', 'PogContentController@getSearchPageList');


Route::post('api/increaseView/{cId}', 'PogContentController@increaseView');


Route::get('api/getPageListDetail/{id}', 'PogContentController@getContentById');


Route::get('api/getContentPageList/{acadType}/{category}/{contentId}', 'PogContentController@getContentByAcadType');

Route::get('api/getPageScore/{id}', 'PogContentController@getPageScore');

   // Route::get(getPageList'getPageList', 'PogContentController');

//PogContentLike

Route::get('api/isLiked/{cId}/{userId}', 'PogContentLikeController@getLiked');



Route::post('api/likeContent/{cId}/{userId}', 'PogContentLikeController@likeContent');

// UserProfile

Route::post('api/setAvatar/{userProfileId}/{avatar}', 'UserProfileController@setAvatar');


//Route::get('api/getPoints/{userProfileId}', 'UserProfileController@getPoints');

Route::get('api/getPoints/{userId}', 'UserRewardsController@getPoints');


Route::post('api/setProfile/{gender}/{avatar}/{hand}/{dob}/{userProfileId}/{fname}/{lname}/{userId}', 'UserProfileController@setProfile');


Route::post('api/increaseGeekos/{userProfileId}', 'UserProfileController@increaseGeekos');


Route::get('api/increaseGeekosDynamic/{point}/{userProfileId}', 'UserProfileController@increaseGeekosDynamic');

/*Route::get('api/increasePoints/{userProfileId}/{pageType}/{gratMedal}/{gratDiamond}/{gratTrophy}', 'UserProfileController@increasePoints');*/

//PogFriends

Route::post('api/addFriends/{userId}/{name}', 'PogFriendsController@addFriends');

//PogNotification

Route::get('api/getNotification', 'NotficationController@getNotification');

//PogEmail

Route::post('api/sendMail/{to}/{wizkidName}/{type}/{message}/{subject}', 'PogEmailController@sendMail');

Route::post('api/deleteMail/{id}/{type}', 'PogEmailController@deleteMail');

Route::get('api/inbox/{wizkidName}', 'PogEmailController@getInbox');


Route::get('api/sent/{wizkidName}', 'PogEmailController@getSent');

//Redeem

Route::get('api/getRedeemList','RedeemController@getRedeemList');

Route::get('api/getRedeemListByCategory/{category}','RedeemController@getRedeemListByCategory');


//Poll

Route::get('api/getPoll/{userId}','PollController@getPoll');

//Birds
Route::get('api/getBirdsList','BirdsController@getBirdsList');
Route::get('api/getBirdById/{id}','BirdsController@getBirdById');
Route::get('api/getOtherBirds/{id}','BirdsController@getOtherBirds');

//UserGeekos

Route::get('api/getTodaysEarnedGeekosCount/{userId}','UserGeekosController@getTodaysEarnedGeekosCount');

Route::get('api/getEarnedGeekosCountHistory/{userId}','UserGeekosController@getEarnedGeekosCountHistory');

Route::get('api/getTodaysSpentGeekosCount/{userId}','UserGeekosController@getTodaysSpentGeekosCount');

Route::get('api/getSpentGeekosCountHistory/{userId}','UserGeekosController@getSpentGeekosCountHistory');

//GeekosMultiplier

Route::get('api/getMultiplierHistory/{userId}','GeekosMultiplierController@getMultiplierHistory');

Route::post('api/multiplyGeekos','GeekosMultiplierController@multiplyGeekos');

Route::post('api/checkGeekosMultiplier','GeekosMultiplierController@checkGeekosMultiplier');

//Avatar

Route::get('api/getAvatarMainFeature/{gender}', 'PogAvatarController@getAvatarMainFeature');


Route::get('api/getRandomAvatarFeatures/{gender}/{eyes}/{lips}/{nose}/{hair}/{top}/{bottom}/{shoe}', 'PogAvatarController@getRandomAvatarFeatures');



Route::get('api/getAvatarSubFeature/{featureType}/{gender}', 'PogAvatarController@getAvatarSubFeature');

Route::get('api/getUserAvatar/{userId}/{gender}','PogUserAvatarController@getUserAvatar');

Route::get('api/saveUserAvatar/{user_id}/{gender}/{eyes_id}/{lips_id}/{nose_id}/{hair_id}/{top_id}/{bottom_id}/{shoe_id}/{facecolor_id}', 'PogUserAvatarController@saveUserAvatar');

Route::get('api/getDefaultUserAvatar/{gender}','PogUserAvatarController@getDefaultUserAvatar');


//UserTime

Route::post('api/trackTime','UserTimeController@trackTime');

//UserRewards

Route::post('api/increasePoints', 'UserRewardsController@increasePoints');

//Unity

Route::post('api/saveRoom', 'UnityController@saveRoom');