<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class UserProfileController extends Controller
{
    //
    public function checkprofilecompletion($userProfileId){
        
        $builder = \App\UserProfile::query();
        
        $builder->where('user_profile_id','=',$userProfileId);
        
       // return $builder->toSql();
        $result = $builder->get();
        return $result;
    }
    
    public function setAvatar($userProfileId, $avatar){
        
        \App\UserProfile::where('user_profile_id', '=', $userProfileId)->update(array('avatar' => $avatar));
        
        
    }
    
    public function getPoints($userProfileId){
        
        $userProfile = \App\UserProfile::where('user_profile_id','=',$userProfileId);
        return $userProfile->get();
    }
    
    public function setProfile($gender,$avatar,$hand,$dob,$userProfileId,$fname,$lname,$userId){
        
         $date = \Datetime::createFromFormat('D M d Y H:i:s e P', $dob)->format('Y-m-d h:i:s'); 
       
        \App\UserProfile::where('user_profile_id', '=', $userProfileId)->update(array('gender'=>$gender,'birthdate' => $date,'dom_hand'=>$hand,'avatar'=>$avatar));
                
        
        \App\Users::where('user_id', '=', $userId)->update(array('fname' => $fname,'lname'=>$lname));
                
    }
    
    public function increaseGeekos($userProfileId){
        
        \App\UserProfile::where('user_profile_id','=', $userProfileId)->increment('geekos',10);
        
        $userProfile = \App\UserProfile::where('user_profile_id','=', $userProfileId)->get();
        
        $userId = $userProfile[0]->user_id;
        
         $userGeekos = \App\UserGeekos::query();
            $userGeekos->where('user_id','=',$userId);
            $userGeekos->where('geekos_earned_spent','=','earned');
            $userGeekos->whereDate('created_at','=', Carbon::today()->toDateString());
            $userGeekosResult = $userGeekos->first();
             
            if(sizeof($userGeekosResult)>0){
                
                $userGeekosResult->increment('geekos_count',10);
            }else{
                $newUserGeekos = new \App\UserGeekos;
                $newUserGeekos->user_id = $userId;
                $newUserGeekos->geekos_count = 10;
                $newUserGeekos->geekos_earned_spent = 'earned';
                $newUserGeekos->save();
            }
        
      return  $userProfile;
    }
    
     public function increaseGeekosDynamic($point,$userProfileId){
        
        \App\UserProfile::where('user_profile_id','=', $userProfileId)->increment('geekos',$point);;
        
      $userProfile =  \App\UserProfile::where('user_profile_id','=', $userProfileId)->get();
         
          $userId = $userProfile[0]->user_id;
        
         $userGeekos = \App\UserGeekos::query();
            $userGeekos->where('user_id','=',$userId);
            $userGeekos->where('geekos_earned_spent','=','earned');
            $userGeekos->whereDate('created_at','=', Carbon::today()->toDateString());
            $userGeekosResult = $userGeekos->first();
             
            if(sizeof($userGeekosResult)>0){
                
                $userGeekosResult->increment('geekos_count',$point);
            }else{
                $newUserGeekos = new \App\UserGeekos;
                $newUserGeekos->user_id = $userId;
                $newUserGeekos->geekos_count = $point;
                $newUserGeekos->geekos_earned_spent = 'earned';
                $newUserGeekos->save();
            }
         
         return  $userProfile;
    }
    
    public function increasePoints($userProfileId, $pageType, $gratMedal, $gratDiamond, $gratTrophy){
        
        $userProfile = \App\UserProfile::where('user_profile_id','=', $userProfileId);
        
        $userProfile->increment('medal',$gratMedal);
        $userProfile->increment('trophy',$gratTrophy);
        $userProfile->increment('diamond',$gratDiamond);
        $userProfile->increment('t_'.$pageType,$gratTrophy);
        $userProfile->increment('m_'.$pageType,$gratMedal);      
        $userProfile->increment('d_'.$pageType,$gratDiamond);
            
        return \App\UserProfile::where('user_profile_id','=',$userProfileId)->get();
        
    }
}