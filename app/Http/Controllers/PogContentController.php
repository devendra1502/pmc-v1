<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PogContentController extends Controller
{
    //
    public function index($type, $category){
        
        $builder = \App\PogContent::query();
       // select('title');
        
        if($type=='home'){
            $builder->where('cont_type','!=','shgame');
        }else{
            $builder->where('cont_type','=',$type);
        }
         $builder->where('is_active','=',1);
        if($category=='top'){
         $builder->orderBy('view','desc');
        }else if($category=='new'){
         $builder->orderBy('created_at','desc');
        } else if($category=='aToZ'){
         $builder->orderBy('title','asc');
        }
        
        $builder->simplePaginate(5);
       // return $builder->toSql();
        $result = $builder->get();
        return $result;
    }
    
    
    public function getFeaturedContent($type){
        
        $builder = \App\PogContent::query();
       // select('title');
        
        if($type=='home'){
            $builder->where('cont_type','!=','shgame');
        }else{
            $builder->where('cont_type','=',$type);
        }
         $builder->where('is_active','=',1);
        $builder->whereNotNull('featured_order');
        $builder->orderBy('featured_order','asc');
       // return $builder->toSql();
        $result = $builder->get();
        return $result;
    }
      public function getStatisticsByDate($to){
        
        
           $averageTimeGeekos = \App\UserProfile::query();
        $averageTimeGeekos->groupBy('gender');
          
          $averageTimeGeekos->select(\DB::raw('gender,avg(geekos)/10 as average_time,avg(geekos) as average_geekos, avg(DATEDIFF( NOW(),birthdate) / 365.25)as avg_age'));
 $averageTimeGeekos->where('created_at', '<=',$to) ;           
          $averageTimeGeekos = $averageTimeGeekos->get();
          
          
           $averageLoginAttempt = \App\UserProfile::query();
          
           $averageLoginAttempt->select(\DB::raw('gender,avg(login_attempt) as average_sessions'));
          $averageLoginAttempt->groupBy('gender');
          $averageLoginAttempt->join('users', function($join){
          $join->on('users.user_id','=','user_profile.user_id');
          });
          $averageLoginAttempt->where('users.created_at', '<=',$to) ;
          
        
          $averageLoginAttempt =  $averageLoginAttempt = $averageLoginAttempt->get();
        
          
          $usersB2B = \App\Users::query();
          $usersB2B->groupBy('is_champuser');
          $usersB2B->select(\DB::raw('is_champuser as is_B2B,count(user_id) as count'));
          $usersB2B->where('created_at', '<=',$to) ;
          $usersB2B = $usersB2B->get();
      
          $isPremium = \App\Users::query();
          $isPremium->groupBy('isPremium');
          $isPremium->select(\DB::raw('isPremium as is_premium,count(user_id) as count'));
          $isPremium->where('created_at', '<=',$to) ;
          $isPremium = $isPremium->get();
      
          
          
          
           $maleCount = \App\UserProfile::query();
        $maleCount->where('gender','=', "male");
             $maleCount->where('created_at', '<=',$to) ;
        $maleCount = $maleCount->count();
           $femaleCount = \App\UserProfile::query();
        $femaleCount->where('gender','=', "female");
           $femaleCount->where('created_at', '<=',$to) ;
        $femaleCount = $femaleCount->count();
     
     $avatarPref = \App\UserProfile::query();
        
      $avatarPref->groupBy('gender','avatar');
     
     $avatarPref->select(\DB::raw('avatar,gender,count(avatar) as count'));
      $avatarPref->where('created_at', '<=',$to) ;
     $avatarPref = $avatarPref->get();
     
     $localeStats = \App\UserProfile::query();
     $localeStats->groupBy('city');
            $localeStats->where('created_at', '<=',$to) ;
     $localeStats = $localeStats->select(\DB::raw('city,count(user_id) as user_count'))->get();

          
          
          return \Response::json(array('avg_time_geekos'=>$averageTimeGeekos, 'avg_session'=>$averageLoginAttempt, 'users_B2B'=>$usersB2B, 'is_premium'=>$isPremium,'male_count'=>$maleCount, 'female_count'=>$femaleCount,'$avatar_pref'=>$avatarPref, "locale_stats"=>$localeStats));
         
    }
    
    
 public function getStatistics(){
 

     
     $series = \App\User::query();
        
      $series->groupBy('created_at');
     
     $series->select(\DB::raw('created_at,count(user_id) as count'));
     
     $series = $series->get();
     
    

     
     return \Response::json(array('series'=>$series));
 }
    
    
        
    public function getFeaturedVideos(){
        
        $builder = \App\PogContent::query();
       // select('title');
     
            $builder->where('cont_type','=','videos');
         $builder->where('is_active','=',1);
        //$builder->whereNotNull('featured_order');
          $builder->orderBy('view','desc');
      //  $builder->orderBy('featured_order','asc');
       // return $builder->toSql();
  $builder->limit(4);
        $result = $builder->get();
         
        return $result;
    }
    
      public function getFeaturedActivities(){
        
        $builder = \App\PogContent::query();
       // select('title');
     
            $builder->where('cont_type','=','activities');
           $builder->where('is_active','=',1);
          $builder->orderBy('view','desc');
      //  $builder->whereNotNull('featured_order');
      //  $builder->orderBy('featured_order','asc');
       // return $builder->toSql();
             $builder->limit(4);
        $result = $builder->get();
        return $result;
    }
    
    
      public function getFeaturedGames(){
        
        $builder = \App\PogContent::query();
       // select('title');
     
            $builder->where('cont_type','=','games');
           $builder->where('is_active','=',1);
          $builder->orderBy('view','desc');
            $builder->limit(4);
       // $builder->whereNotNull('featured_order');
        //$builder->orderBy('featured_order','asc');
       // return $builder->toSql();
        $result = $builder->get();
        return $result;
    }
    
    public function getContentById($id){
        
         $builder = \App\PogContent::query();
        $builder->leftJoin('pog_rewards', function($join){
            $join->on('pog_content.cont_type', '=', 'pog_rewards.content_type');
        });
        
         $builder->where('id','=',$id);
         $builder->where('is_active','=',1);
        
        return $builder->get();
    }
    
    public function getContentByAcadType($acadType, $category,$contentId){
        
         $count = \App\PogContent::query();
        $count->where('id','>',$contentId);
        $count  ->where('acad_type','=',$acadType);
        $count = $count->count();
        
         $builder = \App\PogContent::query();
         $builder->where('acad_type','=',$acadType);
        
        if($count<25){
            
            $builder->where(function($query) use ($contentId){
                    $query->where('id','>', $contentId)
                          ->orWhere('id','<', $contentId);
                });
        }else{
            $builder->where('id','>',$contentId);
        }
        
          $builder->where('is_active','=',1);
                
        if($category=='top'){
         $builder->orderBy('view','desc');
        }else if($category=='new'){
         $builder->orderBy('created_at','desc');
        } else if($category=='aToZ'){
         $builder->orderBy('title','asc');
        }
        
        $builder->simplePaginate(24);
        return $builder->get();
    }
    
    public function getSearchPageList($searchText){
        
        $builder = \App\PogContent::query();
        $builder->orWhere('title', 'like', "$searchText%");
        $builder->orWhere('description', 'like', "$searchText%");
        $builder->orWhere('tags', 'like', "$searchText%");
        $builder->orWhere('source', 'like', "$searchText%");
        $builder->orWhere('acad_type', 'like', "$searchText%");
        $builder->orWhere('cont_type', 'like', "$searchText%");
         $builder->where('is_active','=',1);
        $builder->orderBy('view','desc');
        
        return $builder->get();
    }
    
    public function increaseView($cId){
    //    \App\PogContent::where('id', '=', $cId)->update(array('view' => \DB::raw());
    $view = \App\PogContent::where('id','=',$cId);
                                                                        if(isset($view)){
                                                                            $view->increment('view');
                                                                        }                                   
        
    }
    
    public function getPageScore($id){
        
      return  \App\PogContent::where('id','=', $id)->get();
    }
}