<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AuthenticationController extends Controller
{
    //
    
        public function pmcLogin(Request $request){
        
        $response = array();
        $user = $request->input('customer');
        $password = $user['password'];
        $email = $user['email'];
        
        $puser = \App\ParentsUsers::query()
 ->where('email', $email)
->select('full_name','parents_user_id','email','mobile_number','password')

 ->first();
        
        if(count($puser)>0){
            
            $puserObject =  \App\ParentsUsers::where('parents_user_id','=',$puser->parents_user_id);
            $puserObject->update(array('last_attempt'=>new \DateTime()));
            
            if(md5($password)==$puser->password){
            
            $puserObject =  \App\ParentsUsers::where('parents_user_id','=',$puser->parents_user_id);
            $puserObject->update(array('last_attempt'=>new \DateTime(), 'last_login'=>new \DateTime()));
            $puserObject->increment('login_attempt');
                
                
                
                 $puserObject = \App\ParentsUsers::query()
 ->where('email', $email)
->select('parents_user_id','full_name','email','mobile_number','password','created_at','login_attempt')

 ->first();
            
        $response['status'] = "success";
        $response['message'] = 'Logged in successfully.';
       
        $response['full_name'] = $puserObject['full_name'];
                     $response['parents_user_id'] = $puserObject['parents_user_id'];
             $response['login_attempt'] = $puserObject['login_attempt'];
       
        $response['email'] = $puserObject['email'];
           
        $response['createdAt'] = $puserObject['created_at'];
              $response['mobile_number'] = $puserObject['mobile_number'];   
        if (!isset($_SESSION)) {
            session_start();
        }
        $_SESSION['parents_user_id'] = $puser['parents_user_id'];
                $_SESSION['email'] = $puser['email'];
          $_SESSION['full_name'] = $puser['full_name'];
             $_SESSION['login_attempt'] = $puserObject['login_attempt'];
      
       $_SESSION['mobile_number'] = $puser['mobile_number'];
                
            }else {
            $response['status'] = "error";
            $response['message'] = 'Login failed. Incorrect credentials';
        }
        }else {
            $response['status'] = "error";
            $response['message'] = 'No such user is registered';
        }
    return $response;
        
        
    }
    
    
    
    public function pogLogin(Request $request){
        
        $response = array();
        $user = $request->input('customer');
        $password = $user['password'];
        $wizkidName = $user['wizkid_name'];
        
        $user = \App\Users::query()->join('user_profile', function($join)
 {
   $join->on('users.user_id', '=', 'user_profile.user_id');

 })
 //->select('required column names') 
 ->where('users.wizkid_name', $wizkidName)
->select('users.user_id','users.wizkid_name','users.isPremium','users.login_attempt','users.password','users.created_on','users.email','user_profile.nick_name','user_profile.user_profile_id','user_profile.medal','user_profile.diamond','user_profile.trophy','user_profile.geekos','user_profile.gender','user_profile.birthdate','user_profile.session_time','users.mobile_number')

 ->first();
        
        if(count($user)>0){
            
            $userObject =  \App\Users::where('user_id','=',$user->user_id);
            $userObject->update(array('last_attempt'=>"NOW()"));
            
            if(md5($password)==$user->password){
            
            $userObject =  \App\Users::where('user_id','=',$user->user_id);
            $userObject->update(array('last_attempt'=>"NOW()", 'last_login'=>"NOW()"));
            $userObject->increment('login_attempt');
                
            
        $response['status'] = "success";
        $response['message'] = 'Logged in successfully.';
       
        $response['user_id'] = $user->user_id;
             $response['login_attempt'] = $user['login_attempt'];
        $response['user_profile_id'] = $user['user_profile_id'];
        $response['nick_name'] = $user['nick_name'];
      
        $response['wizkid_name'] = $user['wizkid_name'];
              $response['isPremium'] = $user['isPremium'];
        $response['gender'] = $user['gender'];
        $response['email'] = $user['email'];
             $response['medal'] = $user['medal'];
             $response['diamond'] = $user['diamond'];
             $response['trophy'] = $user['trophy'];
             $response['geekos'] = $user['geekos'];
        $response['createdAt'] = $user['created_on'];
              $response['mobile_number'] = $user['mobile_number'];   
        if (!isset($_SESSION)) {
            session_start();
        }
        $_SESSION['user_id'] = $user['user_id'];
        $_SESSION['email'] = $user['email'];
             $_SESSION['isPremium'] = $user['isPremium'];
       
        $_SESSION['user_profile_id'] = $user['user_profile_id'];
             $_SESSION['login_attempt'] = $user['login_attempt'];
        $_SESSION['nick_name'] = $user['nick_name'];
      
        $_SESSION['gender'] = $user['gender'];
        $_SESSION['birthdate'] = $user['birthdate'];
        $_SESSION['session_time'] = $user['session_time'];
            
       $_SESSION['wizkid_name'] = $user['wizkid_name'];
       $_SESSION['mobile_number'] = $user['mobile_number'];
                
            }else {
            $response['status'] = "error";
            $response['message'] = 'Login failed. Incorrect credentials';
        }
        }else {
            $response['status'] = "error";
            $response['message'] = 'No such user is registered';
        }
    return $response;
        
        
    }
    
     public function pogSignup(Request $request){
        
        $response = array();
        $user = $request->input('customer');
         
         $phone  = $user['mobile_number'];
         $wizkid_name  = $user['wizkid_name'];
         $email  = $user['email'];
    
         $password = $user['password'];
  
         $newUser = new \App\Users;

         $newUser->mobile_number = $phone;
         $newUser->wizkid_name = $wizkid_name;
         $newUser->email = $email;
         $newUser->password = md5($password);   
         $newUser->save();
             
         $newUserProfile = new \App\UserProfile;

         $newUserProfile->user_id = $newUser->user_id;
         $newUserProfile->save();
         
         $newUserGeekos = new \App\UserGeekos;
         
         $newUserGeekos->user_id =  $newUser->user_id;
         $newUserGeekos->geekos_count =  1000;
         $newUserGeekos->geekos_earned_spent =  'earned';
         $newUserGeekos->save();
             
        
        if (count($newUser)>0) {
            
            $response["status"] = "success";
            $response["message"] = "User account created successfully";
            $user = \App\Users::query()->join('user_profile', function($join)
 {
   $join->on('users.user_id', '=', 'user_profile.user_id');

 })->select('users.user_id','users.wizkid_name','users.isPremium','users.login_attempt','users.password','users.created_on','users.email','user_profile.nick_name','user_profile.user_profile_id','user_profile.medal','user_profile.diamond','user_profile.trophy','user_profile.geekos','user_profile.gender','user_profile.birthdate','user_profile.session_time', 'users.mobile_number')

 ->where('user_profile.user_profile_id', $newUserProfile->id)
 ->first();
            
           
            if (count($user)>0) {
            
            
        $response['user_id'] = $user['user_id'];
        $response['user_profile_id'] = $user['user_profile_id'];
        $response['nick_name'] = $user['nick_name'];
       
        $response['gender'] = $user['gender'];
        $response['email'] = $user['email'];
                $response['medal'] = $user['medal'];
                $response['diamond'] = $user['diamond'];
                $response['trophy'] = $user['trophy'];
                $response['geekos'] = $user['geekos'];
                 $response['login_attempt'] = $user['login_attempt'];
                
        $response['createdAt'] = $user['created_on'];
                $response['wizkid_name'] = $user['wizkid_name'];
                 $response['isPremium'] = $user['isPremium'];

            $response['mobile_number'] = $user['mobile_number'];

                $response['session_time'] = $user['session_time'];
            

            }
            if (!isset($_SESSION)) {
                session_start();
            }
              $_SESSION['user_id'] = $user['user_id'];
        $_SESSION['email'] = $email;
     
        $_SESSION['user_profile_id'] = $user['user_profile_id'];
        $_SESSION['nick_name'] = $user['nick_name'];
       
        $_SESSION['gender'] = $user['gender'];
            $_SESSION['birthdate'] = $user['birthdate'];
             $_SESSION['login_attempt'] = $user['login_attempt'];
          
              $_SESSION['wizkid_name'] = $user['wizkid_name'];
             $_SESSION['isPremium'] = $user['isPremium'];

             $_SESSION['mobile_number'] = $user['mobile_number'];
    $_SESSION['session_time'] = $user['session_time'];

           /* require_once 'backend/Mail.class.php';
            $email_message = new email_message();
            $email_message->send($email,'Planet of Gui -  Verification',getRegisterEmail($user['user_id']));
                  $email_message2 = new email_message();
            $email_message2->send("sukhada@shirsa.in",'Yeah!!! New Registration',getRegisterData($user['wizkid_name']));
           */
             global $emailAddress;
              $emailAddress  = $user['email'];
                $subject = 'Planet of Gui -  Verification';
               // $body = $this->getChangePasswordBody();
                
                
                //SEND EMAIL
                \Mail::send('emails.registerEmail', ['userId' => $user['user_id']], function ($message) {
                    global $emailAddress;
                   
    $message->from('support@planetofgui.com', 'Planet of GUI Support');
    $message->subject('Planet of Gui -  Verification');
    $message->to($emailAddress);
                });   
            
            //SEND EMAIL
                \Mail::send('emails.newRegister', ['wizkidName' => $user['wizkid_name']], function ($message) {
                
                   
    $message->from('support@planetofgui.com', 'Planet of GUI Support');
    $message->subject('Yeah!!! New Registration');
    $message->to('sukhada@shirsa.in');
                });   
            
            return $response;
        } else {
            $response["status"] = "error";
            $response["message"] = "Failed to create customer. Please try again";
            return $response;
        }            
    
         
     }
    public function pogLogout(){
        
        $session = $this->destroySession();
        $response["status"] = "info";
        $response["message"] = "Logged out successfully";
        return $response;
    }
      public function pmcLogout(){
        
        $session = $this->destroyPmcSession();
        $response["status"] = "info";
        $response["message"] = "Logged out successfully";
        return $response;
    }
    public function pogSession(){
        
         $session = $this->getSession();
    $response["user_id"] = $session['user_id'];
    $response["email"] = $session['email'];
   
     $response["isPremium"] = $session['isPremium'];
     $response["user_profile_id"] = $session['user_profile_id'];
    $response['nick_name'] = $session['nick_name'];
    $response['wizkid_name'] = $session['wizkid_name'];
    $response['mobile_number'] = $session['mobile_number'];
    $response['gender'] = $session['gender'];
        $response['birthdate'] = $session['birthdate'];
         $response['session_time'] = $session['session_time'];
    $response['login_attempt'] = $session['login_attempt'];
    
    
    return $session;

    }
    
    
    
        public function pmcSession(){
        
         $session = $this->getPmcSession();

    
    return $session;

    }
    
     
    public function getPmcSession(){
    if (!isset($_SESSION)) {
        session_start();
    }
    $sess = array();
    if(isset($_SESSION['parents_user_id']))
    {
        $sess["parents_user_id"] = $_SESSION['parents_user_id'];
       
        $sess["full_name"] = $_SESSION['full_name'];
        $sess["email"] = $_SESSION['email'];
        $sess["login_attempt"] = $_SESSION['login_attempt'];
       
        $sess["mobile_number"] = $_SESSION['mobile_number'];

        
       
       
    }
    else
    {
        $sess["parents_user_id"] = '';
    
        $sess["email"] = '';
        
         $sess["full_name"] = '';
       
        $sess["login_attempt"] = '';
        $sess["mobile_number"] = '';
        
                
// $sess["gender"] = '';
        
    }
    return $sess;
}
    public function getSession(){
    if (!isset($_SESSION)) {
        session_start();
    }
    $sess = array();
    if(isset($_SESSION['user_id']))
    {
        $sess["user_id"] = $_SESSION['user_id'];
       
        $sess["email"] = $_SESSION['email'];
        $sess["login_attempt"] = $_SESSION['login_attempt'];
        $sess["user_profile_id"] = $_SESSION['user_profile_id'];
        $sess["nick_name"] = $_SESSION['nick_name'];
        $sess["wizkid_name"] = $_SESSION['wizkid_name'];
         $sess["isPremium"] = $_SESSION['isPremium'];
        $sess["gender"] = $_SESSION['gender'];
        $sess["birthdate"] = $_SESSION['birthdate'];

        $sess["mobile_number"] = $_SESSION['mobile_number'];

         $sess["session_time"] = $_SESSION['session_time'];
        

       
       
    }
    else
    {
        $sess["user_id"] = '';
          $sess["user_profile_id"] = '';
        
       
        $sess["wizkid_name"] = '';
      
        $sess["email"] = '';
        $sess["nick_name"] = '';
        $sess["gender"] = '';
        $sess["birthdate"] = '';
        $sess["session_time"] = '';
        
                $sess["isPremium"] = '';
         $sess["login_attempt"] = '';
        $sess["mobile_number"] = '';
        
                
// $sess["gender"] = '';
        
    }
    return $sess;
}
    
    public function destroySession(){
    if (!isset($_SESSION)) {
    session_start();
    }
    if(isSet($_SESSION['user_id']))
    {
        unset($_SESSION['user_id']);
        unset($_SESSION['fname']);
        unset($_SESSION['lname']);
        unset($_SESSION['user_profile_id']);
        unset($_SESSION['email']);
        unset($_SESSION['nick_name']);
        unset($_SESSION['gender']);
        unset($_SESSION['birthdate']);
        unset($_SESSION['country']);
         unset($_SESSION['isPremium']);

         unset($_SESSION['mobile_number']);

        unset($_SESSION['session_time']);

       
         unset($_SESSION['login_attempt']);
        $info='info';
        if(isSet($_COOKIE[$info]))
        {
            setcookie ($info, '', time() - $cookie_time);
        }
        $msg="Logged Out Successfully...";
    }
    else
    {
        $msg = "Not logged in...";
    }
    return $msg;
}
public function destroyPmcSession(){
    if (!isset($_SESSION)) {
    session_start();
    }
    if(isSet($_SESSION['parents_user_id']))
    {
        unset($_SESSION['parents_user_id']);
      
        unset($_SESSION['email']);
      
         unset($_SESSION['mobile_number']);
          unset($_SESSION['full_name']);

     

       
         unset($_SESSION['login_attempt']);
        $info='info';
        if(isSet($_COOKIE[$info]))
        {
            setcookie ($info, '', time() - $cookie_time);
        }
        $msg="Logged Out Successfully...";
    }
    else
    {
        $msg = "Not logged in...";
    }
    return $msg;
}

    
    public function pogIsWizkidExist(Request $request){
         
        $user = $request->input('customer');
        $wizkidName = $user['wizkid_name'];
       
       $isUserExists = \App\Users::where('wizkid_name','=', $wizkidName)->first();
        
        if(!$isUserExists){
    $response["status"] = "success";
    $response["message"] = "Wizkid name is valid";
    }else{
    $response["status"] = "error";
    $response["message"] = "Oops! This wizkid name already exists!";
    }
    return $response;
        
    }
}