<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    //
    public function consumeContent($cId, $userId, $ansFlag){
         \App\Users::where('user_id', '=', $userId)->update(array('content_consumed' => \DB::raw("CONCAT(content_consumed,'-',$cId,'*',$ansFlag,'-')")));
                                                                    
    }
       public function routeContent($id,$slug){
         
          return \Redirect::to('/#/content/'.$id.'/'.$slug);
           
    }
      public function routeBirddetail($id){
         
          return \Redirect::to('/#/birddetail/'.$id);
           
    }
     public function routeSearch($text){
         
          return \Redirect::to('/#/search/'.$text);
           
    }
      public function routeProfile($id){
         
          return \Redirect::to('/#/profile/id/'.$id);
           
    }
     public function routeList($type){
         
          return \Redirect::to('/#/list/'.$type);
           
    }
    
    public function routeSubList($type){
         
          return \Redirect::to('/#/sublist/'.$type);
           
    }
    
        public function routeBirds(){
         
          return \Redirect::to('/#/birds');
           
    }
     public function routeSelectavatar(){
         
          return \Redirect::to('/#/selectavatar');
           
    }
     public function routeAvatarselect(){
         
          return \Redirect::to('/#/avatarselect');
           
    }
     public function routeBog(){
         
          return \Redirect::to('/#/bog');
           
    }
     public function routeParents(){
         
          return \Redirect::to('/#/parents');
           
    }
     public function routeSafety(){
         
          return \Redirect::to('/#/safety');
           
    }
     public function routeFaq(){
         
          return \Redirect::to('/#/faq');
           
    }
     public function routeContact(){
         
          return \Redirect::to('/#/contact');
           
    }
     public function routePartners(){
         
          return \Redirect::to('/#/partners');
           
    }
     public function routeForgotpassword(){
         
          return \Redirect::to('/#/forgotpassword');
           
    }
     public function routeChangepassword(){
         
          return \Redirect::to('/#/changepassword');
           
    }
      public function routeStatistics(){
         
          return \Redirect::to('/#/statistics');
           
    }
    
      public function routeChallenge(){
         
          return \Redirect::to('/#/challenge');
           
    }
    
     
    
      public function routeMembership(){
         
          return \Redirect::to('/#/membership');
           
    }
    
      public function routeRedeem(){
         
          return \Redirect::to('/#/redeem');
           
    }
    
      public function routePerformance(){
         
          return \Redirect::to('/#/performance');
           
    }
    
      public function routeShopping(){
         
          return \Redirect::to('/#/shopping');
           
    }
    
      public function routeHelp(){
         
          return \Redirect::to('/#/help');
           
    }
    
      public function routeEmail(){
         
          return \Redirect::to('/#/email');
           
    }
    
    
    
    
    
    
    
    public function submitPoll($questId, $userId, $ansOption){

         \App\Users::where('user_id', '=', $userId)->update(array('poll_answered' => \DB::raw("CONCAT(poll_answered,'-',$questId,'*',$ansOption,'-')")));
        
         $pogPoll = \App\PogPoll::where('poll_id','=',$questId);
        if(isset($pogPoll)){
            $pogPoll->increment('ans_'.$ansOption.'_count');
         }       
                                                                    
    }
    
     public function checkDataConsumption($cId, $userId){
        
        $builder = \App\Users::query()->select('content_consumed');
        $builder->Where('content_consumed', 'like', "%-$cId*%");
        $builder->Where('user_id', '=', $userId);
        
        return $builder->get();
    }
    
     public function getUserDataI($userId){
       // return \App\Users::where('user_id','=',$userId)->with('\userProfile')->get();
         
         return \App\Users::query()->leftJoin('user_profile', function($join)
 {
   $join->on('users.user_id', '=', 'user_profile.user_id');

 })
 //->select('required column names') 
 ->where('users.user_id', $userId)
 ->get();
    }
    
    public function getUserDataN($name){
    
        $user = \App\Users::where('wizkid_name','=', $name);
        
        return $user->get();
    }
    
    public function setuserdata($uHand,$uDob,$userProfileId,$uFname,$uLname,$userId){
      
     
        
        $date = \Datetime::createFromFormat('D M d Y H:i:s e P', $uDob)->format('Y-m-d h:i:s'); 
       
        
        \App\UserProfile::where('user_profile_id', '=', $userProfileId)->update(array('birthdate' => $date,'dom_hand'=>$uHand));
                
        
        \App\Users::where('user_id', '=', $userId)->update(array('fname' => $uFname,'lname'=>$uLname));
                
    }
    
    public function verifyUser($userId){
        
        \App\Users::where('user_id','=', $userId)->update(array('is_verified'=>1));
    }
    
    public function wizkidNames(){
        
        $users = \App\Users::query()->select('wizkid_name');
        
        return $users->get();
    }
    
    public function changePassword($oldPassword, $newPassword, $userId){
        $oldPassword = md5($oldPassword);
        $newPassword = md5($newPassword);
        
        $user = \App\Users::find($userId);
        
        if(count($user)>0){
           
            if($oldPassword==$user->password){
                \App\Users::where('user_id','=', $userId)->update(array('password'=>$newPassword));
                
               global $emailAddress;
              $emailAddress  = $user->email;
                $subject = 'Password Changed - Shirsa.in';
               // $body = $this->getChangePasswordBody();
                
                
                //SEND EMAIL
                \Mail::send('emails.changePassword', ['password' => $newPassword], function ($message) {
                    global $emailAddress;
                   
    $message->from('support@planetofgui.com', 'Planet of GUI Support');
    $message->subject("Password Changed - Shirsa.in");
    $message->to($emailAddress);
                });    
                header("location:/#/signout");
                exit;
            
            }else{
                return  'Old Password Does not match our records.';
            }
            
        }else{
            return 'User does not exist or no active session';
        }
        
        return $user;
    }
    
    public function forgotPassword($wiz){
        
        $user = \App\Users::where('wizkid_name','=',$wiz
                                 )->get();
        
        if(count($user)>0){
            
            //GET RANDOM NUMBER
            $pwd[] = mt_rand(1, 100);
            //GET RANDOM STRING WITH LOWER CASE                     
            $characters = 'abcdefghijklmnopqrstuvwxyz';
            $randomString = '';
            $length=5;
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, strlen($characters) - 1)];
            }
            $pwd[] = $randomString;
            //GET RANDOM STRING WITH CAPITAL CASE
            $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, strlen($characters) - 1)];
            }
            $pwd[] = $randomString;
            //SHUFFLE ARRAY
            shuffle($pwd);
            //MERGE ARRAY TO FORM NEW PASSWORD
            $newPassword = implode("",$pwd);
            
            \App\Users::where('wizkid_name','=',$wiz)->update(array('password'=>md5($newPassword),'is_pwd_sys_generated'=>1));
            
            
            $email= \App\Users::where('wizkid_name','=',$wiz)->select('email')->get();
            
            global $emailAddress;
            $emailAddress = $email[0]->email;
            
          //   $subject = 'New Password - Shirsa.in';
           // $body = $this->getForgotPasswordBody($newPassword);
            
          
            //EMAIL return success error
            \Mail::send('emails.forgotPassword', ['password' => $newPassword], function ($message) {
                global $emailAddress;
    $message->from('support@planetofgui.com', 'Planet of GUI Support');
                $message->subject('New Password - Shirsa.in');
    $message->to($emailAddress);
                }); 
        }else{
            
            //$smarty->assign("ErrForgotPassword","Email is not registerd with us.");
            return "error";
        
        }
        
    }

}