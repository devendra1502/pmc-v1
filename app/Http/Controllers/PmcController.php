<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PmcController extends Controller
{
    //
    public function routeRequestCard(){

        return    \Redirect::to('/#/requestcard');
    }
     public function routeActivateCard(){

        return    \Redirect::to('/#/activatecard');
    }
    
       public function addParent($name, $email, $pass, $mob,$gender){
        
            
                $pmc = new \App\ParentsUsers;
                $pmc->full_name = $name;
                $pmc->email = $email;
                $pmc->password = md5($pass);
          $pmc->mobile_number = $mob;
                $pmc->gender = $gender;
           $pmc->last_attempt = new \DateTime();
             $pmc->last_login = new \DateTime();
             $pmc->login_attempt =1;
                $pmc->created_at = new \DateTime();
                $pmc->save();
           
           
           
           
          
        $password = $pass;
        $email = $email;
        
        $puser = \App\ParentsUsers::query()
 ->where('email', $email)
->select('parents_user_id','full_name','email','mobile_number','password')

 ->first();
        
        if(count($puser)>0){
            
            $puserObject =  \App\ParentsUsers::where('parents_user_id','=',$puser->parents_user_id);
            $puserObject->update(array('last_attempt'=>new \DateTime()));
            
            if(md5($password)==$puser->password){
            
            $puserObject =  \App\ParentsUsers::where('parents_user_id','=',$puser->parents_user_id);
            $puserObject->update(array('last_attempt'=>new \DateTime(), 'last_login'=>new \DateTime()));
            $puserObject->increment('login_attempt');
                
                
                
                 $puserObject = \App\ParentsUsers::query()
 ->where('email', $email)
->select('parents_user_id','full_name','email','mobile_number','password','created_at','login_attempt')

 ->first();
            
        $response['status'] = "success";
        $response['message'] = 'Logged in successfully.';
       
        $response['parents_user_id'] = $puserObject['parents_user_id'];
             $response['login_attempt'] = $puserObject['login_attempt'];
                 $response['full_name'] = $puserObject['full_name'];
       
        $response['email'] = $puserObject['email'];
           
        $response['createdAt'] = $puserObject['created_at'];
              $response['mobile_number'] = $puserObject['mobile_number'];   
        if (!isset($_SESSION)) {
            session_start();
        }
        $_SESSION['parents_user_id'] = $puser['parents_user_id'];
                $_SESSION['email'] = $puser['email'];
                  $_SESSION['full_name'] = $puser['full_name'];
       
             $_SESSION['login_attempt'] = $puserObject['login_attempt'];
      
       $_SESSION['mobile_number'] = $puser['mobile_number'];
                
            }else {
            $response['status'] = "error";
            $response['message'] = 'Login failed. Incorrect credentials';
        }
        }else {
            $response['status'] = "error";
            $response['message'] = 'No such user is registered';
        }
    return $response;
           
           
           
           
           
           
    }
       public function addParentActivate($card,$name, $email, $pass, $mob,$gender){
        
            $card1= \App\PmcCards::query();
   $card1->where('card_number','=', $card);
        $card1->where('is_linked','=', 0);
                
               $card_count=$card1->get(); 
              
                if(count($card_count)>0){
                     
                
                $pmc = new \App\ParentsUsers;
                $pmc->full_name = $name;
                $pmc->email = $email;
                $pmc->password = md5($pass);
          $pmc->mobile_number = $mob;
                $pmc->gender = $gender;
           $pmc->last_attempt = new \DateTime();
             $pmc->last_login = new \DateTime();
             $pmc->login_attempt =1;
                $pmc->created_at = new \DateTime();
                $pmc->save();
           
            $card1= \App\PmcCards::query();
           $card1->where('card_number','=', $card);
                   $card1->update(array('parents_user_id'=>$pmc["parents_user_id"]));   
                    
           
          
        $password = $pass;
        $email = $email;
        
        $puser = \App\ParentsUsers::query()
 ->where('email', $email)
->select('parents_user_id','full_name','email','mobile_number','password')

 ->first();
        
        if(count($puser)>0){
            
            $puserObject =  \App\ParentsUsers::where('parents_user_id','=',$puser->parents_user_id);
            $puserObject->update(array('last_attempt'=>new \DateTime()));
            
            if(md5($password)==$puser->password){
            
            $puserObject =  \App\ParentsUsers::where('parents_user_id','=',$puser->parents_user_id);
            $puserObject->update(array('last_attempt'=>new \DateTime(), 'last_login'=>new \DateTime()));
            $puserObject->increment('login_attempt');
                
                
                
                 $puserObject = \App\ParentsUsers::query()
 ->where('email', $email)
->select('parents_user_id','full_name','email','mobile_number','password','created_at','login_attempt')

 ->first();
            
        $response['status'] = "success";
        $response['message'] = 'Logged in successfully.';
       
                
                
       
        $response['parents_user_id'] = $puserObject['parents_user_id'];
             $response['login_attempt'] = $puserObject['login_attempt'];
                 $response['full_name'] = $puserObject['full_name'];
       
        $response['email'] = $puserObject['email'];
           
        $response['createdAt'] = $puserObject['created_at'];
              $response['mobile_number'] = $puserObject['mobile_number'];   
        if (!isset($_SESSION)) {
            session_start();
        }
        $_SESSION['parents_user_id'] = $puser['parents_user_id'];
                $_SESSION['email'] = $puser['email'];
                  $_SESSION['full_name'] = $puser['full_name'];
       
             $_SESSION['login_attempt'] = $puserObject['login_attempt'];
      
       $_SESSION['mobile_number'] = $puser['mobile_number'];
                
            }else {
            $response['status'] = "error";
            $response['message'] = 'Login failed. Incorrect credentials';
        }
        }else {
            $response['status'] = "error";
            $response['message'] = 'No such user is registered';
        }
             
           
           
           
           
                
                
                }else{
                
                $response['status'] = "error";
            $response['message'] = 'Card not valid';
                
                }
                
                return $response;
  
           
           
    }
    
    public function addNewKid($name, $dob, $pass, $mob,$email,$parent){
        
            
                $pmc = new \App\Users;
                $pmc->wizkid_name = $name;
                $pmc->email = $email;
                $pmc->password = md5($pass);
        $pmc->on_pmc = 1;
          $pmc->mobile_number = $mob;
              
                $pmc->created_at = new \DateTime();
                $pmc->save();
        
        $pmc_profile = new \App\UserProfile;
        $pmc_profile->user_id = $pmc->user_id;
          $pmc_profile->birthdate = $dob;
      
          $pmc_profile->save();
        
        
         $p = new \App\ParentsChild;
        $p->parents_id = $parent;
         
          $p->child_id = $pmc->user_id;
      
          $p->save();
    }
    
    
        public function addKid($name, $pass,$parent){
         
         
            
                     $isUserExists = \App\Users::where('wizkid_name','=', $name)->first();
        
        if(!$isUserExists){
    $response["status"] = "error";
    $response["message"] = "Invalid wizkid name";
           
    }else{
 
              $response["user_id"] = $isUserExists->user_id;
              $puser = \App\ParentsChild::query();
 $puser->where('parents_id', $parent);
          $puser->where('child_id', $response["user_id"]);   
            $p=$puser->get();
            if(count($p)>0){
             
            $response["status"] = "error";
    $response["message"] = "Already Added";
            
            
            }else{
               
          
            $pmc_profile = new \App\ParentsChild;
        $pmc_profile->parents_id = $parent;
              
          $pmc_profile->child_id = $isUserExists->user_id;
     
          $pmc_profile->save();
                
                $isUserExists->update(array('on_pmc'=>1)); 
                  $card1->update(array('is_linked'=>1)); 
                   $response["status"] = "success";
    
            }
            
           
            
            
    }
    return $response;
    }
    
     public function addNewKidActivate($name, $dob, $pass, $mob,$email,$parent){
         
         
        
            
                $pmc = new \App\Users;
                $pmc->wizkid_name = $name;
                $pmc->email = $email;
                $pmc->password = md5($pass);
        $pmc->on_pmc = 1;
          $pmc->mobile_number = $mob;
              
                $pmc->created_at = new \DateTime();
                $pmc->save();
        
        $pmc_profile = new \App\UserProfile;
        $pmc_profile->user_id = $pmc->user_id;
          $pmc_profile->birthdate = $dob;
      
          $pmc_profile->save();
          $card1= \App\PmcCards::query();
                        
           $card1->where('parents_user_id','=', $parent);
                
         $c= $card1->where('is_linked','=', 0)->first(); 
        
         $p = new \App\PmcChildCard;
         
          $p->child_id = $pmc->user_id;
             $p->card_number = $c["card_number"];
             $p->card_pin = $c["card_pin"];
          $p->save();
         

          $card1->update(array('is_linked'=>1));   
                    
         
         
         
    }
    
    
        public function addKidActivate($name, $pass,$parent){
        
            
         
                     $isUserExists = \App\Users::where('wizkid_name','=', $name)->first();
        
        if(!$isUserExists){
    $response["status"] = "error";
    $response["message"] = "Invalid wizkid name";
           
    }else{
 
              $response["user_id"] = $isUserExists->user_id;
              $puser = \App\ParentsChild::query();
 $puser->where('parents_id', $parent);
          $puser->where('child_id', $response["user_id"]);   
            $p=$puser->get();
            if(count($p)>0){
             
            $response["status"] = "error";
    $response["message"] = "Already Added";
            
            
            }else{
               
            $card1= \App\PmcCards::query();
                        
           $card1->where('parents_user_id','=', $parent);
                
         $c= $card1->where('is_linked','=', 0)->first(); 
            $pmc_profile = new \App\PmcChildCard;

          $pmc_profile->child_id = $isUserExists->user_id;
                 $pmc_profile->card_number = $c["card_number"];
             $pmc_profile->card_pin = $c["card_pin"];
          $pmc_profile->save();
       
                
               
                    $card1->update(array('is_linked'=>1));   
                    $response["status"] = "success";
    
            }
            
           
            
            
    }
    return $response;
    }
    
    
     public function isWizkidExist($wizkid){
         
      
       
       $isUserExists = \App\Users::where('wizkid_name','=', $wizkid)->first();
        
        if(!$isUserExists){
    $response["status"] = "success";
    $response["message"] = "Wizkid name is valid";
           
    }else{
    $response["status"] = "error";
    $response["message"] = "Oops! This wizkid name already exists!";
             $response["user_id"] = $isUserExists->user_id;
    }
    return $response;
        
    }
  public function linkParentKid($parent,$kid){
         
         $pmc_profile = new \App\ParentsChild;
        $pmc_profile->parents_id = $parent;
          $pmc_profile->child_id = $kid;
       $pmc_profile->created_at = new \DateTime();
          $pmc_profile->save();
       
      
      
    }
    
    
    public function getKidId($parent){
         
         $pmc_profile = \App\ParentsChild::where('parents_id','=', $parent);
       $pmc_profile->orderBy('updated_at','desc');
       
        return $pmc_profile->first();
    }
    
     public function getKids($parent){
         
         $pmc_profile = \App\ParentsChild::where('parents_id','=', $parent);
       $pmc_profile->orderBy('parents_child.child_id','desc');
       $pmc_profile->join('users', function($join){
       
           $join->on('parents_child.child_id','=','users.user_id');
       });
         $pmc_profile->leftJoin('pmc_child_card', function($join){
       
           $join->on('users.user_id','=','pmc_child_card.child_id');
       });
        return $pmc_profile->get();
    }
        
     public function getPmcUsers(){
         
         $pmc_profile = \App\ParentsChild::where('on_pmc','=', 1);
       $pmc_profile->orderBy('pmc_child_card.child_id','desc');
       $pmc_profile->leftJoin('users', function($join){
       
           $join->on('parents_child.child_id','=','users.user_id');
       });
         $pmc_profile->leftJoin('parents_users', function($join){
       
     $join->on('parents_child.parents_id','=','parents_users.parents_user_id');
       });
          $pmc_profile->leftJoin('pmc_child_card', function($join){
       
     $join->on('parents_child.child_id','=','pmc_child_card.child_id');
       });
         
         
          
       
        return $pmc_profile->get();
    }
     public function getPmcCards(){
         
         $pmc_profile = \App\PmcCards::query();
     
     
        return $pmc_profile->get();
    }
    public function addAddress($line1,$line2,$city,$state,$pin,$parent,$kid){
         
         $pmc_profile = \App\PmcChildCard::query();
        $pmc_profile->where('child_id','=', $kid);
         $pmc_profile->update(array('address_line_1'=>$line1,'address_line_2'=>$line2,'city'=>$city,'state'=>$state,'pin'=>$pin));
      
       
       
      
    }
  
    
    
        
    public function paymentSuccess($parent,$kid){
         
         $pmc_profile = \App\PmcChildCard::query();
        $pmc_profile->where('child_id','=', $kid);
         $pmc_profile->update(array('is_paid'=>1));
      
       
       
      
    }
    
    public function updateTransactionNo($parent,$no){
        
        $childIds = \App\ParentsChild::query();
        $childIds->where('parents_id', '=', $parent);
        $childIds->select('child_id');
         $childIds = $childIds->get();
        
         $childArray = array();
            foreach($childIds as $id){ 
  
                array_push($childArray,$id->child_id);
                
            }
        
         $pmc_profile = \App\PmcChildCard::whereIn('child_id', $childArray);
        
         $pmc_profile->update(array('transactions_per_day'=>$no));
      
       
       
      
    }
    
    
     public function updateAmountPerTransaction($parent,$no){
         
         $childIds = \App\ParentsChild::query();
        $childIds->where('parents_id', '=', $parent);
        $childIds->select('child_id');
         $childIds = $childIds->get();
        
         $childArray = array();
            foreach($childIds as $id){ 
  
                array_push($childArray,$id->child_id);
                
            }
         
         $pmc_profile = \App\PmcChildCard::whereIn('child_id', $childArray);
        
         $pmc_profile->update(array('amount_per_transaction'=>$no));
      
    
    }
    
     public function rechargeCard($card,$amount,$message,$name){
         
        $c= \App\PmcCards::query();
   $c->where('card_number','=', $card);
        $c->where('is_linked','=', 1);
                
               $card_count=$c->get(); 
              
                if(count($card_count)>0){
                    
                      $response['status'] = "success";
            $response['message'] = 'Valid card';
                    
                    
                               }else{
                
                $response['status'] = "error";
            $response['message'] = 'Card not valid';
                
                }
                
                return $response;
  
      
       
       
      
    }
    
    public function checkCard($card,$amount){
         
        $c= \App\PmcCards::query();
   $c->where('card_number','=', $card);
        $c->where('is_linked','=', 1);
                
               $card_count=$c->get(); 
              
                if(count($card_count)>0){
                    
                      $response['status'] = "success";
            $response['message'] = 'Valid card';
                    
                    
                               }else{
                
                $response['status'] = "error";
            $response['message'] = 'Card not valid';
                
                }
                
                return $response;
  
      
       
       
      
    }
    
        public function getTransactionData($parent){
         
            $childIds = \App\ParentsChild::query();
        $childIds->where('parents_id', '=', $parent);

             $childIds = $childIds->get();
            
            $childArray = array();
            foreach($childIds as $id){ 
  
                array_push($childArray,$id->child_id);
                
            }
            
           
         $pmc_profile = \App\PmcChildCard::whereIn('child_id',$childArray);
        
        return  $pmc_profile->first();
      
       
       
      
    }
    
      public function addTransaction($card,$amount,$vendor,$type,$txid){
         
          
               $card1= \App\PmcChildCard::query();
   $card1->where('card_number','=', $card);
      
                
               $card_count=$card1->first(); 
             
                if(count($card_count)>0){
                    
                          $pmc_profile = new \App\PmcTransaction;
                    
                    $pmc_profile->child_id= $card_count["child_id"];
      $pmc_profile->amount=$amount;
          $pmc_profile->name=$vendor;
          $pmc_profile->type=$type;
          $pmc_profile->tx_id=$txid;
                     
                $pmc_profile->save();
//         $pmc_profile->update(array('amount'=>$amount,'name'=>$vendor,'type'=>$type,'tx_id'=>$txid));
                     $response["status"] = "success";
    $response["message"] = "Transaction Added Successfully";
                  
                }else{
                 $response["status"] = "error";
    $response["message"] = "Error adding transaction";
                
                }
       
        
         return $response;
       
       
      
    }
    
    
    
      public function getProfileData($parent){
          $pmc_profile = \App\ParentsUsers::where('parents_user_id','=', $parent);
    
       $pmc_profile->leftJoin('parents_child', function($join){
       
           $join->on('parents_child.parents_id','=','parents_users.parents_user_id');
         });
           
           $pmc_profile->leftJoin('users', function($join){
       
           $join->on('parents_child.child_id','=','users.user_id');
       });
        return $pmc_profile->get();
          
          
          
          
        
      
       
       
      
    }
    
        public function getPassbookData($type,$parentkid){
            
            
            if($type=="all"){
             $pmc_profile = \App\PmcTransaction::query();
            }else{
                $pmc_profile = \App\PmcTransaction::query();
             $pmc_profile->where('type','=', $type);
            }
        
    
   $pmc_profile->where('child_id','=', $parentkid);
        return $pmc_profile->get();
          
          
          
          
        
      
       
       
      
    }
    
    
    
    public function lockCard($id,$reason){
         
         $pmc_profile = \App\PmcChildCard::where('child_id','=', $id);
      
         $pmc_profile->update(array('is_locked'=>1,'locked_reason'=>$reason));
      
       
       
      
    }
     public function unlockCard($id,$reason){
         
         $pmc_profile = \App\PmcChildCard::where('child_id','=', $id);
      
         $pmc_profile->update(array('is_locked'=>0,'unlock_reason'=>$reason));
      
       
       
      
    }
     public function resetPin($id,$old_pin,$new_pin){
         
         $pmc_profile = \App\PmcChildCard::where('child_id','=', $id)->first();
      
         
         if($pmc_profile["card_pin"]==$old_pin){
          $pmc_profile = \App\PmcChildCard::where('child_id','=', $id);
          $pmc_profile->update(array('card_pin'=>$new_pin));
              $p=$pmc_profile->first();
                     $pmc_profile = \App\PmcCards::where('card_number','=', $p->card_number);
          $pmc_profile->update(array('card_pin'=>$new_pin));
              $response["status"] = "success";
    $response["message"] = "Reset successful";
         
         return  $response;
         }else{
          $response["status"] = "error";
    $response["message"] = "Old pin is invalid";
         
         return  $response;
         }
        
      
       
       
      
    }
    
    public function assignCard($childId,$parentsUserId){
         
               $card1= \App\PmcCards::query();
  
        $card1->where('is_linked','=', 0);
                
               $c=  $card1->first(); 
        
         $pmc_profile = new \App\PmcChildCard;
        
         $pmc_profile->child_id = $childId;
             $pmc_profile->card_number = $c->card_number;
             $pmc_profile->card_pin = $c->card_pin;
          $pmc_profile->save();

        
        $card2= \App\PmcCards::query();
  
        $card2->where('id','=',$c->id );
                
              $card2->update(array('is_linked'=>1,'child_id'=>$childId));
    }
    
}