<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserRewardsController extends Controller
{
    
       public function increasePoints(Request $request){
           
           $userId = $request->input('userId');
           $contentId = $request->input('contentId');
           $contentType = $request->input('contentType');
           $pageType = $request->input('pageType');
           $gratMedalCount = $request->input('medalCount');
           $gratDiamondCount = $request->input('diamondCount');
           $gratTrophyCount = $request->input('trophyCount');
               
           if($contentType=='videos'){
               
               if($gratMedalCount!='' && $gratMedalCount>0){
                   
                    $skillType = 'focus';
                   $this->insertPoints($userId, $contentId, 'medal', $skillType, $contentType, $pageType, $gratMedalCount);
                   
                   
               }
               if($gratDiamondCount!='' && $gratDiamondCount>0){
                   
                   $skillType = 'visual';
                    $this->insertPoints($userId, $contentId, 'diamond', $skillType, $contentType, $pageType, $gratDiamondCount);
                   
               }
               if($gratTrophyCount!='' && $gratTrophyCount>0){
                   
                   $skillType = '';
                    $this->insertPoints($userId, $contentId, 'trophy', $skillType, $contentType, $pageType, $gratTrophyCount);
                   
               }
              
               
           }else if($contentType=='games'){
               
                if($gratMedalCount!='' && $gratMedalCount>0){
                   
                    $skillType = 'agility';
                    $this->insertPoints($userId, $contentId, 'medal', $skillType, $contentType, $pageType, $gratMedalCount);
                   
               }
               if($gratDiamondCount!='' && $gratDiamondCount>0){
                   
                   $skillType = 'visual';
                   $this->insertPoints($userId, $contentId, 'diamond', $skillType, $contentType, $pageType, $gratDiamondCount);
                   
               }
               if($gratTrophyCount!='' && $gratTrophyCount>0){
                   
                   $skillType = '';
                   $this->insertPoints($userId, $contentId, 'trophy', $skillType, $contentType, $pageType, $gratTrophyCount);
                   
               }
               
               
           }else if($contentType=='activities'){
               
                if($gratMedalCount!='' && $gratMedalCount>0){
                   
                    $skillType = 'focus';
                    $this->insertPoints($userId, $contentId, 'medal', $skillType, $contentType, $pageType, $gratMedalCount);
                   
               }
               if($gratDiamondCount!='' && $gratDiamondCount>0){
                   
                   $skillType = 'verbal';
                   $this->insertPoints($userId, $contentId, 'diamond', $skillType, $contentType, $pageType, $gratDiamondCount);
                   
               }
               if($gratTrophyCount!='' && $gratTrophyCount>0){
                   
                   $skillType = '';
                   $this->insertPoints($userId, $contentId, 'trophy', $skillType, $contentType, $pageType, $gratTrophyCount);
                   
               }
               
               
           }
           
            $userRewards = \App\UserRewards::query();
           $userRewards->select(\DB::raw('sum(amount) as amount, reward_type'));
            $userRewards->where('user_id','=',$userId);
            
            $userRewards->groupBy('reward_type');
            return $userRewards->get();
        
    }
    
    public function insertPoints($userId, $contentId, $rewardType, $skillType, $contentType, $pageType, $amount){
        
          $userRewards = \App\UserRewards::query();
           $userRewards->where('user_id','=', $userId);
           $userRewards->where('content_id','=', $contentId);
           $userRewards->where('reward_type','=', $rewardType);
           $userRewards->where('skill_type','=', $skillType);
           $userRewards->where('content_type','=', $contentType);
           $userRewards->where('page_type','=', $pageType);
           $userRewards->where(\DB::raw('Date(created_at)'),'=',(new \DateTime())->format('Y-m-d'));
           
        if(count($userRewards->get())>0){
            
            $userRewards->increment('amount', $amount);
                
        }else{
            
            $newUserRewards = new \App\UserRewards();
            
            $newUserRewards->user_id = $userId;
            $newUserRewards->content_id = $contentId;
            $newUserRewards->reward_type = $rewardType;
            $newUserRewards->skill_type = $skillType;
            $newUserRewards->content_type = $contentType;
            $newUserRewards->page_type = $pageType;
            $newUserRewards->amount = $amount;
            
            $newUserRewards->save();
        }  
            
        
    }
    
    public function getPoints($userId){
        
        
        $userRewards = \App\UserRewards::query();
        $userRewards->select(\DB::raw('user_rewards.user_id, reward_type, page_type, sum(amount) as amount'));
       
        $userRewards->where('user_rewards.user_id', '=', $userId);
        
        $userRewards->groupBy('reward_type');
        $userRewards->groupBy('page_type');
        
        $userRewards =  $userRewards->get();
        
        $resultArray = array();
        foreach($userRewards as $reward){
            
            $value = ($reward->reward_type.'_'.$reward->page_type);
            
           $arrayy = array($value=>$reward->amount);
          
            $resultArray = array_merge($resultArray, $arrayy);
        }
        
         $userRewardsByRewardType = \App\UserRewards::query();
        $userRewardsByRewardType->select(\DB::raw('reward_type, sum(amount) as amount'));
        
       /* $userRewardsByRewardType->leftJoin('user_profile', function($join){
            
            $join->on('user_profile.user_id', '=', 'user_rewards.user_id');
        });*/
       
        $userRewardsByRewardType->where('user_rewards.user_id', '=', $userId);
        
        $userRewardsByRewardType->groupBy('reward_type');
        
        $userRewardsByRewardType = $userRewardsByRewardType->get();
        
        $resultArray2 = array();
        foreach($userRewardsByRewardType as $reward){
            
           $arrayy2 = array($reward->reward_type=>$reward->amount);
          
            $resultArray2 = array_merge($resultArray2, $arrayy2);
        }
        
        $userProfile = \App\UserProfile::query();
        $userProfile->where('user_id','=', $userId);
        $userProfile = $userProfile->get();
     
            $createdAt = $userProfile[0]->created_at;
            $geeoks = $userProfile[0]->geekos;
            
    
        
         return \Response::json(array("rewardsByPageType"=>$resultArray, "rewardsByRewardType"=>$resultArray2,"created_at"=>$createdAt, "geekos"=>$geeoks));
        
    }
    
}