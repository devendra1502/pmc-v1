<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserTimeController extends Controller
{
    public function trackTime(Request $request){
        
        $userTimeTracker = $request->input('userTimeTracker');
        $userId = $userTimeTracker['user_id'];
        $time = $userTimeTracker['time'];
        $contentType = $userTimeTracker['content_type'];
        $acadType = $userTimeTracker['acad_type'];
        $contentId = $userTimeTracker['content_id'];
        
        $userTime  = \App\UserTime::query();
        $userTime->where('user_id','=',$userId);
        $userTime->where(\DB::raw('Date(created_at)'),'=',(new \DateTime())->format('Y-m-d'));
         $userTime->where('content_type','=',$contentType);
        $userTime->where('acad_type','=',$acadType);
        $userTime->where('content_id','=',$contentId);
                
         
        if(!(count($userTime->get())>0)){
            
            $newUserTime = new \App\UserTime(); 
            $newUserTime->user_id = $userId;
            $newUserTime->time_spent = $time;
            $newUserTime->content_type = $contentType;
            $newUserTime->acad_type = $acadType;
            $newUserTime->content_id = $contentId;
            
            $newUserTime->save();
        }else{
            
            $userTime->increment('time_spent',$time);
            
        }
        
    }
}