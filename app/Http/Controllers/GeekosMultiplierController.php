<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class GeekosMultiplierController extends Controller
{
    //
    public function getMultiplierHistory($userId){
        
        $geekosMultiplier = \App\GeekosMultiplier::query();
        $geekosMultiplier->where('user_id','=',$userId);
        $geekosMultiplier->orderBy(\DB::raw('Date(created_at)'),'desc');
        $geekosMultiplier->limit(5);
        return $geekosMultiplier->get();
    }
    
    public function multiplyGeekos(Request $request){
        $userId = $request->input('userId');
        $geekosCount = $request->input('geekosCount');
        $weekCount = $request->input('weekCount');
        
        $isActive = \App\GeekosMultiplier::query();
        $isActive->where('user_id','=',$userId);
        $isActive->where('is_active','=','1');
        $isActive = $isActive->get();
        
        
        if($isActive->isEmpty()){
        
        $geekosMultiplier = new \App\GeekosMultiplier;
        $geekosMultiplier->user_id = $userId;
        $geekosMultiplier->geekos_count = $geekosCount;
        $geekosMultiplier->week_count = $weekCount;
        $geekosMultiplier->is_active = '1';
            
        $geekosMultiplier->save();
            
        }else{
            return 'Already one in process';
        }
    }
    
    public function checkGeekosMultiplier(Request $request){
        
        $userId = $request->input('userId');
        
        $isActive = \App\GeekosMultiplier::query();
        $isActive->where('user_id','=',$userId);
        $isActive->where('is_active','=','1');
       // $isActive->where('created_at','<', Carbon::now()->subWeeks(\DB::raw('geekos_multiplier.week_count')));
        $isActive->whereRaw('geekos_multiplier.created_at < NOW() - INTERVAL geekos_multiplier.week_count WEEK');
        $isActive = $isActive->first();
        
        if($isActive){
            //update multiplier is active
            $isActive->is_active = 0;
            $isActive->save();
            
            if($isActive->week_count==1){
                $geekosCount = $isActive->geekos_count*1.25;
            }else if($isActive->week_count==2){
                $geekosCount = $isActive->geekos_count*1.5;
            }else if($isActive->week_count==3){
                $geekosCount = $isActive->geekos_count*1.75;
            }else if($isActive->week_count==4){
                $geekosCount = $isActive->geekos_count*2;
            }
            
            
            
            //increment user geekos
            \App\UserProfile::where('user_id','=', $userId)->increment('geekos',$geekosCount);
        
            //increment daily geekos count
             $userGeekos = \App\UserGeekos::query();
            $userGeekos->where('user_id','=',$userId);
            $userGeekos->where('geekos_earned_spent','=','earned');
            $userGeekos->whereDate('created_at','=', Carbon::today()->toDateString());
            $userGeekosResult = $userGeekos->first();
             
            if(sizeof($userGeekosResult)>0){
                
                $userGeekosResult->increment('geekos_count',$geekosCount);
            }else{
                $newUserGeekos = new \App\UserGeekos;
                $newUserGeekos->user_id = $userId;
                $newUserGeekos->geekos_count = $geekosCount;
                $newUserGeekos->geekos_earned_spent = 'earned';
                $newUserGeekos->save();
            }
            
            return $geekosCount;
        }else{
            return 'no increment';
        }
    }
}
