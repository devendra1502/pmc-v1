<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PogAvatarController extends Controller
{
    
   public function getAvatarMainFeature($gender){
       
       $avatar = \App\PogAvatar::query();
       
       $avatar->where('feature_category','=','main_feature');
       $avatar->where('gender','=',$gender);
       
       return $avatar->get();
       
   }
    
    
        public function getAvatarSubFeature($featureType, $gender){
       
       $avatar = \App\PogAvatar::query();
       
       $avatar->where('feature_category','=','sub_feature');
       $avatar->where('feature_type','=',$featureType);
       $avatar->where('gender','=',$gender);
       return $avatar->get();
       
   }
      public function getRandomAvatarFeatures( $gender,$eyes,$lips,$nose,$hair,$top,$bottom,$shoe){
       
       $avatar = \App\PogAvatar::query();
       
       $avatar->whereOr('random_id','=',$gender.'-top-'.$top);
        $avatar->whereOr('random_id','=',$gender.'-eyes-'.$eyes);
           $avatar->whereOr('random_id','=',$gender.'-lips-'.$lips);
           $avatar->whereOr('random_id','=',$gender.'-nose-'.$nose);
           $avatar->whereOr('random_id','=',$gender.'-hair-'.$hair);
           $avatar->whereOr('random_id','=',$gender.'-bottom-'.$bottom);
            $avatar->whereOr('random_id','=',$gender.'-shoe-'.$shoe);
          
          
          
       $avatar->where('gender','=',$gender);
       return $avatar->get();
       
   }
    
}