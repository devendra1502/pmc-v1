<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class RedeemController extends Controller
{
    //
    public function getRedeemList(){
        
        $redeem = \App\Redeem::query();
        return $redeem->get();
    }
    
   public function getRedeemListByCategory($category){
       
       $redeem = \App\Redeem::where('is'.$category,'=',1);
       return $redeem->get();
   }
}
