<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserGeekosController extends Controller
{
    //
    public function getTodaysEarnedGeekosCount($userId){
        
        $dt = new \DateTime();
        
        $geekosCount = \App\UserGeekos::query();
        $geekosCount->select('geekos_count');
        $geekosCount->where('user_id','=',$userId);
        $geekosCount->where('geekos_earned_spent','=','earned');
        $geekosCount->where(\DB::raw('Date(created_at)'),'=',$dt->format('Y-m-d'));
        return $geekosCount->first();
    }
    
    public function getEarnedGeekosCountHistory($userId){
                
        $geekosCount = \App\UserGeekos::query();
        $geekosCount->select('geekos_count','created_at');
        $geekosCount->where('user_id','=',$userId);
        $geekosCount->where('geekos_earned_spent','=','earned');
        $geekosCount->orderBy(\DB::raw('Date(created_at)'),'desc');
        $geekosCount->limit(10);
        return $geekosCount->get();
    }
    
    public function getTodaysSpentGeekosCount($userId){
        
        $dt = new \DateTime();
        
        $geekosCount = \App\UserGeekos::query();
        $geekosCount->select('geekos_count');
        $geekosCount->where('user_id','=',$userId);
        $geekosCount->where('geekos_earned_spent','=','spent');
        $geekosCount->where(\DB::raw('Date(created_at)'),'=',$dt->format('Y-m-d'));
        return $geekosCount->first();
    }
    
     public function getSpentGeekosCountHistory($userId){
                
        $geekosCount = \App\UserGeekos::query();
        $geekosCount->select('geekos_count','created_at');
        $geekosCount->where('user_id','=',$userId);
        $geekosCount->where('geekos_earned_spent','=','spent');
        $geekosCount->orderBy(\DB::raw('Date(created_at)'),'desc');
        $geekosCount->limit(10);
        return $geekosCount->get();
    }
}
