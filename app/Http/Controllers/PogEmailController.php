<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PogEmailController extends Controller
{
    //
    
    public function sendMail($to, $wizkidName, $type, $message,$subject){
        
            
                $pogEmail = new \App\PogEmail;
                $pogEmail->friend_name = $to;
                $pogEmail->wizkid_name = $wizkidName;
                $pogEmail->message_type = $type;
          $pogEmail->message_subject = $subject;
                $pogEmail->message = $message;
                $pogEmail->creation_date = "NOW()";
                $pogEmail->save();
    }
     public function deleteMail($id, $type){
        
            
                $pogEmail = \App\PogEmail::find($id);
         if($type=='inbox'){
             $pogEmail->inbox_is_active = 0;
         }else if($type=='outbox'){
              $pogEmail->outbox_is_active = 0;
         }
         
              
                $pogEmail->update();
    }
    
    public function getInbox($wizkidName){
        
    $pogEmail = \App\PogEmail::query();
           $pogEmail->where('friend_name','=',$wizkidName);
        $pogEmail->where('inbox_is_active','=','1');
        $pogEmail->orderBy('created_at','desc');
        return $pogEmail->get();
    }
    
    public function getSent($wizkidName){
         $pogEmail = \App\PogEmail::query();
       $pogEmail->where('wizkid_name','=',$wizkidName);
          $pogEmail->orderBy('created_at','desc');
          $pogEmail->where('outbox_is_active','=','1');
        return $pogEmail->get();
    }
}