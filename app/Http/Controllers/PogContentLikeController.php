<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PogContentLikeController extends Controller
{
    //
    public function getLiked($cId, $userId){
        
         $builder = \App\PogContentLike::query();
         $builder->where('user_id','=',$userId);
         $builder->where('content_id','=',$cId);
         $checkLike = $builder->get();
        
        if(count($checkLike)>0){
            return "disliked";
        }else{
            return "liked";
        }
        
    }
    
    public function likeContent($cId, $userId){
        
        $builder = \App\PogContentLike::query();
        
        $builder->where('user_id','=',$userId);
        $builder->where('content_id','=',$cId);
        $result = $builder->get();
        
        if(count($result)>0){
            
            \App\PogContentLike::where('user_id', '=', $userId)->where('content_id','=',$cId)->delete();
            
             $view = \App\PogContent::where('id','=',$cId);
                                                                            if(isset($view)){
                                                                            $view->decrement('like_count');
                                                                            }                                   
        return "disliked";
            
        }else{
            $pogContentLike = new \App\PogContentLike;
            
            $pogContentLike->user_id = $userId;
            $pogContentLike->content_id = $cId;
            $pogContentLike->save();
            
            $view = \App\PogContent::where('id','=',$cId);
                                                                            if(isset($view)){
                                                                                $view->increment('like_count');
                                                                            }                                   
    
            return "liked";
        
        
    }
}
}