<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PollController extends Controller
{
    //
     public function getPoll($userId){
        
         if($userId==0){
             $builder = \App\PogPoll::query();
                   
             return $builder->get();
         }
         
        $builder = \App\Users::query()->select('poll_answered');
        $pollAnswered = $builder->Where('user_id', '=', $userId)->get();
        $keywords = explode('*', $pollAnswered);
      
         $questIds = array();
        foreach($keywords as $key)
        {
            array_push($questIds,substr($key, -1));
        }
    
         unset($questIds[count($questIds)-1]);

        $builder = \App\PogPoll::query();
        $builder->WhereNotIn('poll_id', $questIds);
            
        return $builder->get();
    }
}
