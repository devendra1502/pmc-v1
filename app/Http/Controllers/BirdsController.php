<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class BirdsController extends Controller
{
    //
    public function getBirdsList(){
        
        $birds = \App\PogBirds::query();
        return $birds->get();
    }
    
   public function getBirdById($id){
       
       $birds = \App\PogBirds::where('id','=',$id);
       return $birds->get();
   }
     public function getOtherBirds($id){
       
       $birds = \App\PogBirds::where('id','!=',$id);
       return $birds->get();
   }
}