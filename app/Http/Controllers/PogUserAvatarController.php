<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PogUserAvatarController extends Controller
{
    public function getUserAvatar($userId, $gender){
       
       $avatar = \App\PogUserAvatar::query();
       
       $avatar->where('user_id','=',$userId);
        
        $avatar = $avatar->get();
    
        if (count($avatar)>0){
                        return $avatar;
        }
       else{

        
          return  'avatarNotSet';
           
           
            
       }
   }
    
    public function getDefaultUserAvatar($gender){
          $defaultAvatar = \App\PogAvatar::query();
           if ($gender== 'boy'){
                $defaultAvatar->whereIn('pog_avatar_id',array(2,22,27,32,37,42,47));
           }else{
                $defaultAvatar->whereIn('pog_avatar_id',array(52,57,62,67,72,77,82));
           }
        $defaultAvatar = $defaultAvatar->get();
        return $defaultAvatar;
    }
    
    public function saveUserAvatar($userId, $gender, $hairId, $eyesId, $noseId, $lipsId, $topId, $bottomId, $shoeId, $facecolorId){
        $avatar = \App\PogUserAvatar::query();
        $avatar->where('user_id', '=', $userId);
        $avatar = $avatar->get();
       
        if (!count($avatar)>0){
            $saveAvatar = new \App\PogUserAvatar;
                $saveAvatar->user_id = $userId;
                $saveAvatar->gender = $gender;
                $saveAvatar->hair_id = $hairId;
                $saveAvatar->eyes_id = $eyesId;
                $saveAvatar->nose_id = $noseId;
                $saveAvatar->lips_id = $lipsId;
                $saveAvatar->top_id = $topId;
                $saveAvatar->bottom_id = $bottomId;
                $saveAvatar->shoe_id = $shoeId;
                $saveAvatar->facecolor_id = $facecolorId;
            $saveAvatar->save();
        }else{
            $saveAvatar = \App\PogUserAvatar::where('user_id', '=', $userId);
      
              $saveAvatar->update(array('user_id'=>$userId, 'gender'=>$gender, 'hair_id'=>$hairId, 'eyes_id'=>$eyesId, 'nose_id'=>$noseId, 'lips_id'=>$lipsId, 'top_id'=>$topId, 'bottom_id'=>$bottomId, 'shoe_id'=>$shoeId, 'facecolor_id'=>$facecolorId));
            
           
        }
        
            
    }
       
}