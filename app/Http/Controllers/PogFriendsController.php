<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PogFriendsController extends Controller
{
    //
    public function addFriends($userId, $name){
        
        $user = \App\Users::where('wizkid_name','=', $name)->get();
        
        if(count($user)>0){
            
            $pogFriends = \App\PogFriends::where('wizkid_id','=',$userId)->where('friend_name','=', $name)->get();
            
            if(count($pogFriends)>0){
                
        \App\PogFriends::where('wizkid_id', '=', $userId)->where('friend_name','=',$name)->update(array('isFriend' => 1));
        
            }else{
                $pogFriend = new \App\PogFriends;
                $pogFriend->wizkid_id = $userId;
                $pogFriend->friend_name = $name;
                $pogFriend->isFriend = 1;
                $pogFriend->updation_date = "NOW()";
                $pogFriend->save();
                
               // \App\PogFriends::create(array('wizkid_id' =>$userId,'friend_name'=>$name,'is_friend'=>1,'updation_date'=>"NOW()"));
            }
        }else{
            return "NRF";
        }   
        
    }
}
