<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PogContent extends Model
{
    //
    protected $table = 'pog_content';
    
    protected $guarded = ['*'];
}
