<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserTime extends Model
{
    //
    
     protected $table = 'user_time';
    
    protected $guarded = ['*'];
    
    protected $primaryKey = 'user_time_id';
}