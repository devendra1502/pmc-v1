<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PogContentLike extends Model
{
    //
    protected $table = 'pog_content_like';
    
    protected $guarded = ['*'];
}
