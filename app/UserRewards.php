<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRewards extends Model
{
    protected $table = 'user_rewards';
    
    protected $guarded = ['*'];
    
    protected $primaryKey = 'reward_id';
}