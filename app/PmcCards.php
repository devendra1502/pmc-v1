<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PmcCards extends Model
{
    //
    protected $table = 'pmc_cards';
    
    protected $guarded = ['*'];
}