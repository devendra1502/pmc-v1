<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PogEmail extends Model
{
    //
    protected $table = 'pog_email';
    
    protected $guarded = ['*'];
}
