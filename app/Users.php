<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users extends \Eloquent
{
    
    protected $table = 'users';
    
    protected $primaryKey = 'user_id';
    //
    public function userProfile()
    {
        return $this->hasOne('\App\UserProfile','user_id','user_id');
    }
    
   
}
