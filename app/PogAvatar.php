<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PogAvatar extends Model
{
    protected $table = 'pog_avatar';
    
    protected $guarded = ['*'];
}