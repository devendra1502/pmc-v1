<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PmcChildCard extends Model
{
    //
    protected $table = 'pmc_child_card';
    
    protected $guarded = ['*'];
}
