<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParentsUsers extends \Eloquent
{
    
    protected $table = 'parents_users';
    
    protected $primaryKey = 'parents_user_id';
    
}