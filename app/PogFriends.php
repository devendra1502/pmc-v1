<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PogFriends extends \Eloquent
{
    //
    protected $table = 'pog_friends';
    
    protected $guarded = array();
}
